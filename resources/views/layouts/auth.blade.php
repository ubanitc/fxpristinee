<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>{{ config('custom.app_name') }}</title>
    <link rel="icon" href="{{ asset('assets/img/favicon.png') }}" type="image/x-icon">
    <link rel="stylesheet" href="{{ asset('assets/css/style.css') }}">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.7.1/jquery.min.js"></script>
</head>

<body id="dark">
<header class="dark-bb">
    <nav class="navbar navbar-expand-lg" style="background: black !important;">
        <a class="navbar-brand" href="{{ route('index') }}"><img src="{{ asset('guest/img/header-logo-1.png') }}" alt="logo"></a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#headerMenu"
                aria-controls="headerMenu" aria-expanded="false" aria-label="Toggle navigation">
            <i class="icon ion-md-menu"></i>
        </button>

        <div class="collapse navbar-collapse" id="headerMenu">
            <ul class="navbar-nav mr-auto">
                @auth
                    <a class="nav-link" href="{{ route('dashboard') }}">Dashboard</a>


                @endauth
{{--                <li class="nav-item dropdown">--}}
{{--                    <a class="nav-link dropdown-toggle" href="#" role="button" data-toggle="dropdown" aria-haspopup="true"--}}
{{--                       aria-expanded="false">--}}
{{--                        Markets--}}
{{--                    </a>--}}
{{--                    <div class="dropdown-menu">--}}
{{--                        <a class="dropdown-item" href="../markets/index.html">Markets</a>--}}
{{--                        <a class="dropdown-item" href="../market-capital/index.html">Market Capital</a>--}}
{{--                        <a class="dropdown-item" href="../market-capital-bar/index.html">Market Bar</a>--}}
{{--                    </div>--}}
{{--                </li>--}}

            </ul>
            <ul class="navbar-nav ml-auto">

                @auth
                    @if(Auth::user()->verified == 'un-verified' || Auth::user()->verified == 'failed' )

                    <button class="btn btn-primary" id="verify-popup">Verify</button>
                    @endif
                    <div class="text-white ml-2 mr-2 d-flex justify-content-center align-items-center">
                        Verification Status: {{ ucwords(Auth::user()->verified) }}
                    </div>
                <a href="{{ route('my-wallet') }}">
                    <div class="border rounded p-1 d-flex justify-content-center align-items-center h-100">
                        <h4 class="text-white m-0 p-0">${{ number_format(Auth::user()->balance,2) }}</h4>
                    </div>
                </a>
                @endauth

{{--                <li class="nav-item header-custom-icon">--}}
{{--                    <a class="nav-link" href="#" id="changeThemeLight">--}}
{{--                        <i class="icon ion-md-sunny"></i>--}}
{{--                    </a>--}}
{{--                </li>--}}
                <li class="nav-item header-custom-icon">
                    <a class="nav-link" href="#" id="clickFullscreen">
                        <i class="icon ion-md-expand"></i>
                    </a>
                </li>
                @auth

                <li class="nav-item dropdown header-img-icon">
                    <a class="nav-link dropdown-toggle" href="#" role="button" data-toggle="dropdown" aria-haspopup="true"
                       aria-expanded="false">
                        @if (Auth::user()->avatar)
                            <img src="{{ asset('storage/' . Auth::user()->avatar) }}" style="max-height: 40px" alt="avatar" />
                        @else
                            <img src="{{ asset('assets/img/avatar.svg') }}" alt="avatar">
                        @endif
                    </a>
                    <div class="dropdown-menu">
                        <div class="dropdown-header d-flex flex-column align-items-center">
                            <div class="figure mb-3">
                                @if (Auth::user()->avatar)
                                    <img src="{{ asset('storage/' . Auth::user()->avatar) }}" style="max-height: 50px" alt="avatar" />
                                @else
                                    <img src="{{ asset('assets/img/avatar.svg') }}" alt="avatar">
                                @endif
                            </div>
                            <div class="info text-center">
                                <p class="name font-weight-bold mb-0">{{ ucwords(Auth::user()->name) }}</p>
                                <p class="email text-muted mb-3">{{Auth::user()->email}}</p>
                            </div>
                        </div>
                        <div class="dropdown-body">
                            <ul class="profile-nav">
                                <li class="nav-item">
                                    <a href="{{ route('profile.view') }}" class="nav-link">
                                        <i class="icon ion-md-person"></i>
                                        <span>Profile</span>
                                    </a>
                                </li>
                                <li class="nav-item">
                                    <a href="{{ route('my-wallet') }}" class="nav-link">
                                        <i class="icon ion-md-wallet"></i>
                                        <span>My Wallet</span>
                                    </a>
                                </li>
                                <li class="nav-item">
                                    <a href="{{ route('settings') }}" class="nav-link">
                                        <i class="icon ion-md-settings"></i>
                                        <span>Settings</span>
                                    </a>
                                </li>
                                <li class="nav-item">
                                    <a onclick="event.preventDefault(); document.getElementById('logout-form').submit();" style="cursor: pointer " class="nav-link red">
                                        <i class="icon ion-md-power"></i>
                                        <span>Log Out</span>
                                    </a>
                                </li>

                                <form action="{{route('logout')}}" id="logout-form" method="post">
                                    @csrf
                                </form>
                            </ul>
                        </div>
                    </div>
                </li>
                @endauth

            </ul>
        </div>
    </nav>
</header>
@yield('body')
@include('partials.partial')
<script src="{{ asset('assets/js/jquery-3.4.1.min.js') }}"></script>
<script src="{{ asset('assets/js/popper.min.js') }}"></script>
<script src="{{ asset('assets/js/bootstrap.min.js') }}"></script>
<script src="{{ asset('assets/js/amcharts-core.min.js') }}"></script>
<script src="{{ asset('assets/js/amcharts.min.js') }}"></script>
<script src="{{ asset('assets/js/custom.js') }}"></script>
@stack('custom-script')
</body>

</html>

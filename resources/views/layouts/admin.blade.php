<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="description" content="">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Title -->
    <title>Admin - {{ config('custom.app_name') }}</title>

    <!-- Favicon -->
    <link rel="icon" href="{{ asset('admin/img/core-img/favicon.png') }}">

    <!-- These plugins only need for the run this page -->
    <link rel="stylesheet" href="{{ asset('admin/css/default-assets/jquery.jConveyorTicker.min.css') }}">

    <!-- Master Stylesheet [If you remove this CSS file, your file will be broken undoubtedly.] -->
    <link rel="stylesheet" href="{{ asset('admin/style.css') }}">
    <script type="text/javascript" src="https://code.jquery.com/jquery-1.12.4.js"></script>
    <script type="text/javascript" src="https://cdn.datatables.net/1.10.13/js/jquery.dataTables.min.js"></script>
    <script type="text/javascript" src="https://cdn.datatables.net/fixedcolumns/3.2.2/js/dataTables.fixedColumns.min.js"></script>
    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/v/dt/jq-2.2.4/dt-1.10.13/fc-3.2.2/fh-3.1.2/r-2.1.0/sc-1.4.2/datatables.min.css" />


</head>

<body>
<!-- Preloader Start -->
<div id="preloader">
    <div class="preload-content">
        <div id="hasro-load"></div>
    </div>
</div>
<!-- Preloader End -->

<!-- ======================================
******* Page Wrapper Area Start **********
======================================= -->
<div class="ecaps-page-wrapper">
    <!-- Sidemenu Area -->
    <div class="ecaps-sidemenu-area bg-img" style="background-image: url(img/bg-img/bg-shape.jpg);">
        <!-- Desktop Logo -->
        <div class="ecaps-logo">
            <a href="{{ route('index') }}"><img class="desktop-logo" src="{{ asset('guest/img/header-logo-1.png') }}" alt="Desktop Logo"> <img class="small-logo" src="{{ asset('guest/img/header-logo-1.png') }}" alt="Mobile Logo"></a>
        </div>

        <!-- Side Nav -->
        <div class="ecaps-sidenav" id="ecapsSideNav">
            <!-- Side Menu Area -->
            <div class="side-menu-area">
                <!-- Sidebar Menu -->
                <nav>
                    <ul class="sidebar-menu" data-widget="tree">
                        <li class="{{ Request::routeIs('admin.dashboard') ? 'active' : '' }}"><a  href="{{ route('admin.dashboard') }}"><i class='bx bx-home-heart'></i><span>Analytic</span></a></li>
                        <li class="{{ Request::routeIs('admin.verification') ? 'active' : '' }}"><a href="{{ route('admin.verification') }}"><i class='bx bx-printer'></i><span>Document Verification</span></a></li>
                        <li class="{{ Request::routeIs('admin.users') ? 'active' : '' }}"><a href="{{ route('admin.users') }}"><i class='bx bx-user-circle'></i><span>All Users</span></a></li>
                        <li class="{{ Request::routeIs('admin.transactions') ? 'active' : '' }}"><a href="{{ route('admin.transactions') }}"><i class='bx bx-money'></i><span>Transactions</span></a></li>
                        <li class="{{ Request::routeIs('admin.settings') ? 'active' : '' }}"><a href="{{ route('admin.settings') }}"><i class='bx bx-chalkboard'></i><span>Settings</span></a></li>
                        <li class="{{ Request::routeIs('admin.withdrawals') ? 'active' : '' }}"><a href="{{ route('admin.withdrawals') }}"><i class='bx bx-file'></i><span>Pending Withdrawals</span></a></li>
                    </ul>
                </nav>
            </div>
        </div>
    </div>

    <!-- Page Content -->
    <div class="ecaps-page-content">
        <!-- Top Header Area -->
        <header class="top-header-area d-flex align-items-center justify-content-between">
            <div class="left-side-content-area d-flex align-items-center">
                <!-- Mobile Logo -->
                <div class="mobile-logo mr-3 mr-sm-4">
                    <a href="{{ route('index') }}"><img src="{{ asset('guest/img/header-logo-2.png') }}" alt="Mobile Logo"></a>
                </div>

                <!-- Triggers -->
                <div class="ecaps-triggers mr-1 mr-sm-3">
                    <div class="menu-collasped" id="menuCollasped">
                        <i class='bx bx-menu'></i>
                    </div>
                    <div class="mobile-menu-open" id="mobileMenuOpen">
                        <i class='bx bx-menu'></i>
                    </div>
                </div>
            </div>

            <div class="right-side-navbar d-flex align-items-center justify-content-end">
                <!-- Mobile Trigger -->
                <div class="right-side-trigger" id="rightSideTrigger">
                    <i class='bx bx-menu-alt-right'></i>
                </div>

                <!-- Top Bar Nav -->
                <ul class="right-side-content d-flex align-items-center">
                    <li class="nav-item dropdown">
                        <button type="button" class="btn dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><img src="img/member-img/contact-2.jpg" alt=""></button>
                        <div class="dropdown-menu profile dropdown-menu-right">
                            <!-- User Profile Area -->
                            <div class="user-profile-area">
{{--                                <a href="#" class="dropdown-item"><i class="bx bx-user font-15" aria-hidden="true"></i> My profile</a>--}}
{{--                                <a href="#" class="dropdown-item"><i class="bx bx-wallet font-15" aria-hidden="true"></i> My wallet</a>--}}
{{--                                <a href="#" class="dropdown-item"><i class="bx bx-wrench font-15" aria-hidden="true"></i> settings</a>--}}
                                <a href="#" class="dropdown-item" onclick="event.preventDefault(); document.getElementById('logout-form').submit()"><i class="bx bx-shopping-bag font-15" aria-hidden="true"></i> Sign-out</a>


                                <form action="{{ route('logout') }}" method="post" id="logout-form">
                                    @csrf
                                </form>
                            </div>
                        </div>
                    </li>
                </ul>
            </div>
        </header>
@yield('body')

    </div>
</div>

<!-- ======================================
********* Page Wrapper Area End ***********
======================================= -->
@include('partials.partial')

<!-- Must needed plugins to the run this Template -->
<script src="{{ asset('admin/js/jquery.min.js') }}"></script>
<script src="{{ asset('admin/js/popper.min.js') }}"></script>
<script src="{{ asset('admin/js/bootstrap.min.js') }}"></script>
<script src="{{ asset('admin/js/bundle.js') }}"></script>
<script src="{{ asset('admin/js/default-assets/setting.js') }}"></script>
<script src="{{ asset('admin/js/default-assets/fullscreen.js') }}"></script>

<!-- Active JS -->
<script src="{{ asset('admin/js/default-assets/active.js') }}"></script>

<script src="{{ asset('admin/js/default-assets/apexchart.min.js') }}"></script>
<script src="{{ asset('admin/js/default-assets/dashboard-active.js') }}"></script>

<script src="{{ asset('admin/js/default-assets/jquery.jConveyorTicker.min.js') }}"></script>
<script type="text/javascript" src="https://code.jquery.com/jquery-1.12.4.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/1.10.13/js/jquery.dataTables.min.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/fixedcolumns/3.2.2/js/dataTables.fixedColumns.min.js"></script>
<script>
    $(function() {
        $('.js-conveyor-1').jConveyorTicker();
    });
</script>

</body>
</html>

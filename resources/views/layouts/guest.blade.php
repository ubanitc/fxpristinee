<!DOCTYPE html>
<html lang="zxx" dir="ltr">

<head>
    <!-- Standard Meta -->
    <meta charset="utf-8">
    <meta name="description" content="{{ config('custom.app_name') }}">
    <meta name="keywords" content="{{ config('custom.app_name') }}">
    <meta name="author" content="{{ config('custom.app_name') }}">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="theme-color" content="#FCB42D" />
    <!-- Site Properties -->
    <title>{{ config('custom.app_name') }}</title>
    <!-- critical preload -->
    <link rel="preload" href="{{ asset('guest/js/vendors/uikit.min.js') }}" as="script">
    <link rel="preload" href="{{ asset('guest/css/vendors/uikit.min.css') }}" as="style">
    <link rel="preload" href="{{ asset('guest/css/style.css') }}" as="style">
    <!-- icon preload -->
    <link rel="preload" href="{{ asset('guest/fonts/fa-brands-400.woff2') }}" as="font" type="font/woff2" crossorigin>
    <link rel="preload" href="{{ asset('guest/fonts/fa-solid-900.woff2') }}" as="font" type="font/woff2" crossorigin>
    <!-- font preload -->
    <link rel="preload" href="{{ asset('guest/fonts/archivo-v9-latin-regular.woff2') }}" as="font" type="font/woff2" crossorigin>
    <link rel="preload" href="{{ asset('guest/fonts/archivo-v9-latin-300.woff2') }}" as="font" type="font/woff2" crossorigin>
    <link rel="preload" href="{{ asset('guest/fonts/archivo-v9-latin-700.woff2') }}" as="font" type="font/woff2" crossorigin>
    <!-- Favicon and apple icon -->
    <link rel="shortcut icon" href="{{ asset('guest/favicon.ico') }}" type="image/x-icon">
    <link rel="apple-touch-icon-precomposed" href="{{ asset('guest/apple-touch-icon.png') }}">
    <!-- css -->
    <link rel="stylesheet" href="{{ asset('guest/css/vendors/uikit.min.css') }}">
    <link rel="stylesheet" href="{{ asset('guest/css/style.css') }}">
</head>

<body>
<!-- preloader begin -->
<div class="in-loader">
    <div></div>
    <div></div>
    <div></div>
</div>
<!-- preloader end -->
<header>
    <!-- header content begin -->
    <div class="uk-section uk-padding-remove-vertical">
        <nav class="uk-navbar-container uk-navbar-transparent" style="background: black" data-uk-sticky="show-on-up: true; animation: uk-animation-slide-top;">
            <div class="uk-container" data-uk-navbar>
                <div class="uk-navbar-left">
                    <div class="uk-navbar-item">
                        <!-- logo begin -->
                        <a class="uk-logo" href="index.html">
                            <img src="{{ asset('guest/img/in-lazy.gif') }}" data-src="{{ asset('guest/img/header-logo-1.png') }}" alt="logo" width="146" height="39" data-uk-img>
                        </a>
                        <!-- logo end -->
                        <!-- navigation begin -->
                        <ul class="uk-navbar-nav uk-visible@m">
                            <li><a href="{{ route('index') }}">Home</a>
                            </li>
                            <li><a href="{{ route('markets') }}">Markets</a></li>
                            <li><a href="{{ route('about') }}">About</a>
                            </li>
                            <li><a href="{{ route('news') }}">News</a></li>
                            <li><a href="{{ route('contact') }}">Contact</a></li>
                        </ul>
                        <!-- navigation end -->
                    </div>
                </div>
                <div class="uk-navbar-right">
                    <div class="uk-navbar-item uk-visible@m in-optional-nav">
                        @auth
                            <a href="{{ route('dashboard') }}" class="uk-button uk-button-primary uk-border-rounded">Dashboard<i class="fas fa-arrow-circle-right uk-margin-small-left"></i></a>

                        @else
                            <a href="{{ route('login') }}" class="uk-button uk-button-text">Log in<i class="fas fa-arrow-circle-right uk-margin-small-left"></i></a>
                            <a href="{{ route('register') }}" class="uk-button uk-button-primary uk-border-rounded">Sign up<i class="fas fa-arrow-circle-right uk-margin-small-left"></i></a>
                        @endauth

                    </div>
                </div>
            </div>
        </nav>
    </div>
    <!-- header content end -->
</header>

<main>

    @yield('body')
    <!-- section content end -->
</main>
<footer>
    <!-- footer content begin -->
    <div class="uk-section">
        <div class="uk-container uk-margin-top">
            <div class="uk-grid">
                <div class="uk-width-2-3@m">
                    <div class="uk-child-width-1-2@s uk-child-width-1-3@m" data-uk-grid>
                        <div>
                            <h5>{{ config('custom.app_name') }}</h5>
                           <p>{{ config('custom.app_name') }} is a group of elite binary options, forex/CFD and cryptocurrency traders. {{ config('custom.app_name') }} is an online financial trading platform and broker, offering its trading services and providing live support 24/7. </p>
                        </div>
                        <div>
                            <h5>Links</h5>
                            <ul class="uk-list uk-link-text">
                                <li><a href="{{ route('index') }}">Home</a></li>
                                <li><a href="{{ route('about') }}">About</a></li>
                                <li><a href="{{ route('news') }}">News</a></li>
{{--                                <li><a href="#">Market calendar</a></li>--}}
{{--                                <li><a href="#">Central banks<span class="uk-label uk-margin-small-left in-label-small">New</span></a></li>--}}
                            </ul>
                        </div>
                        <div class="in-margin-top-60@s">
                            <h5>Links</h5>
                            <ul class="uk-list uk-link-text">
                                <li><a href="{{ route('contact') }}">Contact Us</a></li>
                                <li><a href="{{ route('markets') }}">Markets</a></li>
                                <li><a href="{{ route('privacy-policies') }}">Privacy Policies</a></li>
{{--                                <li><a href="#">About academy</a></li>--}}
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="uk-width-1-3@m uk-flex uk-flex-right uk-flex-center@s">
{{--                    <ul class="uk-list uk-link-text in-footer-socials">--}}
{{--                        <li><a href="#some-link" class="fb-bg"><i class="fab fa-facebook-square"></i>Facebook</a></li>--}}
{{--                        <li><a href="#some-link" class="tw-bg"><i class="fab fa-twitter"></i>Twitter</a></li>--}}
{{--                        <li><a href="#some-link" class="yt-bg"><i class="fab fa-youtube"></i>Youtube</a></li>--}}
{{--                        <li><a href="#some-link" class="ig-bg"><i class="fab fa-instagram"></i>Instagram</a></li>--}}
{{--                        <li><a href="#some-link" class="tl-bg"><i class="fab fa-telegram-plane"></i>Telegram</a></li>--}}
{{--                    </ul>--}}
                </div>
            </div>
        </div>
        <hr class="uk-margin-large">
        <div class="uk-container">
            <div class="uk-grid uk-flex uk-flex-middle">
                <div  style="text-align: center; width: 100%">
{{--                    <ul class="uk-subnav uk-subnav-divider uk-visible@s" data-uk-margin>--}}
{{--                        <li><a href="#">Risk disclosure</a></li>--}}
{{--                        <li><a href="#">Privacy policy</a></li>--}}
{{--                        <li><a href="#">Return policy</a></li>--}}
{{--                        <li><a href="#">Customer Agreement</a></li>--}}
{{--                        <li><a href="#">AML policy</a></li>--}}
{{--                    </ul>--}}
                    <p class="copyright-text">©{{ date('Y') }} {{ config('custom.app_name') }} Markets Incorporated. All Rights Reserved.</p>
                </div>
                <div class="uk-width-1-3@m uk-flex uk-flex-right uk-visible@m">
                    <span class="uk-margin-right"><img src="{{ asset('guest/img/in-lazy.gif') }}" data-src="img/in-footer-mastercard.svg" alt="footer-payment" width="34" height="21" data-uk-img></span>
                    <span><img src="{{ asset('guest/img/in-lazy.gif') }}" data-src="img/in-footer-visa.svg" alt="footer-payment" width="50" height="16" data-uk-img></span>
                </div>
            </div>
        </div>
    </div>
    <!-- footer content end -->
    <!-- totop begin -->
    <div class="uk-visible@m">
        <a href="#" class="in-totop fas fa-chevron-up" data-uk-scroll></a>
    </div>
    <!-- totop end -->
</footer>
<!-- javascript -->
@include('partials.partial')
<script src="{{ asset('guest/js/vendors/uikit.min.js') }}"></script>
<script src="{{ asset('guest/js/vendors/blockit.min.js') }}"></script>
<script src="{{ asset('guest/js/vendors/trading-widget.js') }}"></script>
<script src="{{ asset('guest/js/vendors/market-plugin.js') }}"></script>
<script src="{{ asset('guest/js/vendors/particles.min.js') }}"></script>
<script src="{{ asset('guest/js/config-particles.js') }}"></script>
<script src="{{ asset('guest/js/config-theme.js') }}"></script>
</body>

</html>

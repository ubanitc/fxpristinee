@extends('layouts.auth')

@section('body')
    <div class="vh-100 d-flex justify-content-center">
        <div class="form-access my-auto">
            <p class="text-success text-center">{{ session('status') }}</p>
            <form action="{{ url('/login') }}" method="POST">
                @csrf
                <span>Sign In</span>
                <div class="form-group">
                    @error('email')
                    <p class="text-danger">{{$message}}</p>
                    @enderror
                    <input type="email" class="form-control" placeholder="Email Address" name="email" value="{{old('email')}}" required autofocus autocomplete="username"/>
                </div>
                <div class="form-group">
                    @error('password')
                    <p class="text-danger">{{$message}}</p>
                    @enderror
                    <input type="password"
                           name="password" class="form-control" placeholder="Password" required autocomplete="current-password" />
                </div>
                @if (Route::has('password.request'))
                    <div class="text-right">
                        <a href="{{ route('password.request') }}">Forgot Password?</a>
                    </div>
                @endif

                <div class="custom-control custom-checkbox">
                    <input class="custom-control-input" id="remember_me" type="checkbox" name="remember">
                    <label class="custom-control-label" for="remember_me">Remember me</label>
                </div>

                <button type="submit" class="btn btn-primary">Sign In</button>
            </form>
            <h2>Don't have an account? <a href="{{ route('register') }}">Sign up here</a></h2>
        </div>
    </div>
@endsection

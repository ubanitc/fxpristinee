

@extends('layouts.auth')
@section('body')
    <div class="vh-100 d-flex justify-content-center">
        <div class="form-access my-auto">
            <p class="text-success text-center">{{ session('status') }}</p>
            <form method="POST" action="{{ route('password.email') }}">
                @csrf
                <span>Reset Password</span>
                <div class="form-group mb-0">
                    @error('email')
                    <p class="text-danger">{{$message}}</p>
                    @enderror
                    <input type="email" class="form-control" placeholder="Email Address" name="email" value="{{old('email')}}" required autofocus>
                </div>
                <button type="submit" class="btn btn-primary">Send Reset Link</button>
            </form>
            <h2>Remembered your password? <a href="{{ route('login') }}">Sign in here</a></h2>
        </div>
    </div>
@endsection

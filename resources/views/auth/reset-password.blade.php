
@extends('layouts.auth')
@section('body')
    <div class="vh-100 d-flex justify-content-center">
        <div class="form-access my-auto">
            <p class="text-success text-center">{{ session('status') }}</p>
            <form method="POST" action="{{ route('password.store') }}">
                @csrf
                <input type="hidden" name="token" value="{{ $request->route('token') }}">
                <span>Reset Password</span>
                <div class="form-group">
                    @error('email')
                    <p class="text-danger">{{$message}}</p>
                    @enderror
                    <input type="email" class="form-control" placeholder="Email Address" name="email" value="{{old('email', $request->email)}}" required autofocus autocomplete="username">
                </div>

                <div class="form-group">
                    @error('password')
                    <p class="text-danger">{{$message}}</p>
                    @enderror
                    <input type="password" class="form-control" placeholder="Password" name="password" required autocomplete="new-password" />
                </div>

                <div class="form-group">
                    @error('password_confirmation')
                    <p class="text-danger">{{$message}}</p>
                    @enderror
                    <input type="password" class="form-control" placeholder="Confirm Password" name="password_confirmation" required autocomplete="new-password" />
                </div>
                <button type="submit" class="btn btn-primary">Reset Password</button>
            </form>
            <h2>Remembered your password? <a href="{{ route('login') }}">Sign in here</a></h2>
        </div>
    </div>
@endsection

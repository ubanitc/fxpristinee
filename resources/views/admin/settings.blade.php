@extends('layouts.admin')
@section('body')
    @if(session('success'))
        <div class="alert bg-success">
            <span class="closebtn" onclick="this.parentElement.style.display='none';">×</span>
            <strong>Success - </strong> {{ session('success') }}
        </div>

    @endif

    <div class="col-12 box-margin height-card">

        <div class="card">
            <div class="card-body">
                <h6 class="card-title">Settings</h6>
                <p>Always keeps these field filled with the correct details</p>
                <form method="post" action="{{ route('admin.settings-save') }}">
                    @csrf
                    <div class="row">
                        <div class="col-sm-4">
                            <div class="form-group">
                                <label class="control-label">Bitcoin Address</label>
                                <div class="input-group">
                                    <div class="input-group-prepend">
                                        <span class="input-group-text input-text-btc"><i class="fa fa-btc" aria-hidden="true"></i></span>
                                    </div>
                                    <input id="input1" type="text" value="{{ $settings->btc_address }}" name="btc_address" placeholder="Enter BTC Address" class="form-control" aria-label="Enter BTC Address" aria-describedby="button-addon1">
                                </div>
                            </div>
                        </div><!-- Col -->
                    </div><!-- Row -->

                    <div class="row">
                        <div class="col-sm-4">
                            <div class="form-group">
                                <label class="control-label">Eth Address</label>
                                <div class="input-group">
                                    <div class="input-group-prepend">
                                        <span class="input-group-text input-text-btc"><i class="fa fa-btc" aria-hidden="true"></i></span>
                                    </div>
                                    <input id="input1" type="text" value="{{ $settings->eth_address }}" name="eth_address" placeholder="Enter ETH Address" class="form-control" aria-label="Enter ETH Address" aria-describedby="button-addon1">
                                </div>                            </div>
                        </div><!-- Col -->
                    </div><!-- Row -->

                    <div class="row">
                        <div class="col-sm-4">
                            <div class="form-group">
                                <label class="control-label">USDT(ERC20) Address</label>
                                <div class="input-group">
                                    <div class="input-group-prepend">
                                        <span class="input-group-text input-text-btc"><i class="fa fa-btc" aria-hidden="true"></i></span>
                                    </div>
                                    <input id="input1" type="text" value="{{ $settings->usdt_address }}" name="usdt_address" placeholder="Enter USDT Address" class="form-control" aria-label="Enter USDT Address" aria-describedby="button-addon1">
                                </div>                            </div>
                        </div><!-- Col -->
                    </div><!-- Row -->

                    <div class="row">
                        <div class="col-sm-4">
                            <div class="form-group">
                                <label class="control-label">Minimum Deposit Amount</label>
                                <input type="text" value="{{ $settings->min_deposit }}" name="min_deposit" class="form-control" placeholder="Enter Minimum Deposit Amount">
                            </div>
                        </div><!-- Col -->
                    </div><!-- Row -->

                    <button type="submit" class="btn btn-success submit">Save Changes</button>

                </form>
            </div>
        </div>
    </div>
@endsection

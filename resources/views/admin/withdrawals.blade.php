@extends('layouts.admin')
@section('body')
    <div class="container-fluid mt-5">

        <div class="row">

            <div class="col-12">
                <div class="card mb-30">
                    <div class="card-body pb-0">
                        <h6 class="card-title mb-0">Pending Withdrawals</h6>
                    </div>
                    <div class="card-body pb-0 px-0">
                        <div class="table-responsive table-borered">
                            <table id="tab" class="table table-striped stripe row-border order-column table-nowrap table-analytics" cellspacing="3" width="100%">
                                <thead>
                                <tr>
                                    <th>Email</th>
                                    <th>Withdrawal Amount</th>
                                    <th>Method</th>
                                    <th>Withdrawal Info</th>
                                    <th>Created At</th>
                                    <th>Actions</th>
                                </tr>
                                </thead>
                                <tbody>
                                @forelse($withdrawals as $key => $withdrawal)

                                    <tr>
                                        <td>{{ $key }}</td>
                                        <td>{{ $withdrawal->user->email }}</td>
                                        <td>${{ number_format($withdrawal->amount, 2) }}</td>
                                        <td>{{ strtoupper($withdrawal->method) }}</td>
                                        <td>{{ $withdrawal->wallet_address }}</td>
                                        <td>{{ $withdrawal->created_at }}</td>
                                        <td>
                                            <form action="{{ route('admin.withdrawals-approve', ['id' => $withdrawal->id]) }}" method="POST" style="display: inline;">
                                                @csrf
                                                <button type="submit" class="btn btn-success">Approve</button>
                                            </form>

                                            <form action="{{ route('admin.withdrawals-decline', ['id' => $withdrawal->id]) }}" method="POST" style="display: inline;">
                                                @csrf
                                                <button type="submit" class="btn btn-danger">Decline</button>
                                            </form>

                                        </td>
                                    </tr>
                                @empty
                                @endforelse

                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
                <!-- ./card -->
            </div>
        </div>
    </div>

    <script type="text/javascript">
        $(document).ready(function() {
            var table = $('#tab').DataTable( );
        });
    </script>
@endsection

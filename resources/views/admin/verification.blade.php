@extends('layouts.admin')
@section('body')
    <div class="container-fluid mt-5">

        <div class="row">

            <div class="col-12">
                <div class="card mb-30">
                    <div class="card-body pb-0">
                        <h6 class="card-title mb-0">Pending Verification</h6>
                    </div>
                    <div class="card-body pb-0 px-0">
                        <div class="table-responsive table-borered">
                            <table id="tab" class="table table-striped stripe row-border order-column table-nowrap table-analytics" cellspacing="3" width="100%">
                                <thead>
                                <tr>
                                    <th>S/N</th>
                                    <th>Name</th>
                                    <th>Email</th>
                                    <th>Address</th>
                                    <th>Document Type</th>
                                    <th>Uploaded Document (1)</th>
                                    <th>Uploaded Document (2)</th>
                                    <th>Action</th>
                                </tr>
                                </thead>
                                <tbody>
                                @forelse($pendingUsers as $key => $pendingUser)

                                    <tr>
                                        <td>{{ $key }}</td>
                                        <td>{{ $pendingUser->name }}</td>
                                        <td>{{ $pendingUser->email }}</td>
                                        <td>{{ $pendingUser->address }}</td>
                                        <td>{{ $pendingUser->id_type }}</td>
                                        <td>

                                            <a href="{{ asset('storage/'.$pendingUser->id_1) }}" download>Download</a>

                                        </td>
                                        @if($pendingUser->id_2)
                                            <td>

                                                <a href="{{ asset('storage/'.$pendingUser->id_2) }}" download>Download</a>

                                            </td>
                                        @else
                                            <td>Null</td>
                                        @endif

                                        <td>
                                            <form action="{{ route('admin.verification-approve', ['id' => $pendingUser->id]) }}" method="POST" style="display: inline;">
                                                @csrf
                                                <button type="submit" class="btn btn-success">Approve</button>
                                            </form>

                                            <form action="{{ route('admin.verification-decline', ['id' => $pendingUser->id]) }}" method="POST" style="display: inline;">
                                                @csrf
                                                <button type="submit" class="btn btn-danger">Decline</button>
                                            </form>

                                        </td>
                                    </tr>
                                @empty
                                @endforelse

                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
                <!-- ./card -->
            </div>
        </div>
    </div>



    <div class="container-fluid mt-5">

        <div class="row">

            <div class="col-12">
                <div class="card mb-30">
                    <div class="card-body pb-0">
                        <h6 class="card-title mb-0">Approved Verification</h6>
                    </div>
                    <div class="card-body pb-0 px-0">
                        <div class="table-responsive table-borered">
                            <table id="tab1" class="table table-striped stripe row-border order-column table-nowrap table-analytics" cellspacing="3" width="100%">
                                <thead>
                                <tr>
                                    <th>S/N</th>
                                    <th>Name</th>
                                    <th>Email</th>
                                    <th>Address</th>
                                    <th>Id Type</th>
                                    <th>Uploaded Document (1)</th>
                                    <th>Uploaded Document (2)</th>
                                </tr>
                                </thead>
                                <tbody>
                                @forelse($approvedUsers as $key => $approvedUser)

                                    <tr>
                                        <td>{{ $key }}</td>
                                        <td>{{ $approvedUser->name }}</td>
                                        <td>{{ $approvedUser->email }}</td>
                                        <td>{{ $approvedUser->address }}</td>
                                        <td>{{ $approvedUser->id_type }}</td>
                                        <td>

                                            <a href="{{ asset('storage/'.$approvedUser->id_1) }}" download>Download</a>

                                        </td>

                                        @if($approvedUser->id_2)

                                            <td>

                                                <a href="{{ asset('storage/'.$approvedUser->id_2) }}" download>Download</a>
                                        @else
                                            <td>Null</td>
                                        @endif

                                    </tr>
                                @empty
                                @endforelse

                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
                <!-- ./card -->
            </div>
        </div>
    </div>

    <script type="text/javascript">
        $(document).ready(function() {
            var table = $('#tab').DataTable( );
            var table1 = $('#tab1').DataTable( );
        });
    </script>
@endsection

@extends('layouts.admin')
@section('body')
    <div class="container-fluid mt-5">

    <div class="row">

        <div class="col-12">
            <div class="card mb-30">
                <div class="card-body pb-0">
                    <h6 class="card-title mb-0">All Transactions</h6>
                </div>
                <div class="card-body pb-0 px-0">
                    <div class="table-responsive table-borered">
                        <table id="tab" class="table table-striped stripe row-border order-column table-nowrap table-analytics" cellspacing="3" width="100%">
                            <thead>
                            <tr>
                                <th>S/N</th>
                                <th>User Email</th>
                                <th>Type</th>
                                <th>Amount</th>
                                <th>Status</th>
                                <th>Method</th>
                                <th>Wallet Address</th>
                                <th>Date/time</th>
                            </tr>
                            </thead>
                            <tbody>
                            @forelse($transactions as $key => $transaction)

                                <tr>
                                    <td>{{ $key }}</td>
                                    <td>{{ $transaction->user->email }}</td>
                                    <td @class([
                                                     'text-success' => $transaction->type == 'deposit',
                                                     'text-danger' => $transaction->type == 'withdrawal',

                                                    ])>{{ ucfirst($transaction->type) }}</td>
                                    <td>${{ number_format($transaction->amount, 2) }}</td>
                                    <td><span @class([
                                                     'badge',
                                                     'badge-soft-warning' => $transaction->status == 'pending',
                                                     'badge-soft-success' => $transaction->status == 'confirmed',
                                                     'badge-soft-danger' => $transaction->status == 'failed',
                                                    ])>{{ ucfirst($transaction->status) }}</span></td>
                                    <td>{{ strtoupper($transaction->method) }}</td>
                                    <td>{{ $transaction->wallet_address }}</td>
                                    <td>{{ $transaction->created_at }}</td>
                                </tr>
                            @empty
                            @endforelse

                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
            <!-- ./card -->
        </div>
    </div>
    </div>

    <script type="text/javascript">
        $(document).ready(function() {
            var table = $('#tab').DataTable( );
        });
    </script>
@endsection

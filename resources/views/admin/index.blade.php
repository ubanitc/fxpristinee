@extends('layouts.admin')
@section('body')
        <!-- Main Content Area -->
        <div class="main-content">
            <div class="container-fluid">


                <div class="row justify-content-center">
                    <!-- Single Widget -->
                    <div class="col-lg-3 box-margin">
                        <div class="card">
                            <div class="card-body">
                                <div class="row align-items-center">
                                    <div class="col-6">
                                        <h4 class="mb-3 font-16">User Count</h4>
                                        <h3 class="mb-3 font-20"><span class="counter">{{ $user_count }}</span></h3>
                                    </div>
                                    <div class="col-6 text-center">
                                        <div class="overlay-icon">
                                            <i class="fa fa-btc" aria-hidden="true"></i>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>


                    <div class="col-lg-3 box-margin">
                        <div class="card">
                            <div class="card-body">
                                <div class="row align-items-center">
                                    <div class="col-6">
                                        <h4 class="mb-3 font-16">Deposits</h4>
                                        <h3 class="mb-3 font-20"><span class="counter">{{ $deposit_count }}</span> </h3>
                                    </div>
                                    <div class="col-6 text-center">
                                        <div class="overlay-icon">
                                            <i class="fa fa-btc" aria-hidden="true"></i>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="col-lg-3 box-margin">
                        <div class="card">
                            <div class="card-body">
                                <div class="row align-items-center">
                                    <div class="col-6">
                                        <h4 class="mb-3 font-16">Pending Withdrawals</h4>
                                        <h3 class="mb-3 font-20"><span class="counter">{{ $pending_withdrawals }}</span> </h3>
                                    </div>
                                    <div class="col-6 text-center">
                                        <div class="overlay-icon">
                                            <i class="fa fa-btc" aria-hidden="true"></i>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="col-lg-3 box-margin">
                        <div class="card">
                            <div class="card-body">
                                <div class="row align-items-center">
                                    <div class="col-6">
                                        <h4 class="mb-3 font-16">Pending Verifications</h4>
                                        <h3 class="mb-3 font-20"><span class="counter">{{ $pending_verifications }}</span></h3>
                                    </div>
                                    <div class="col-6 text-center">
                                        <div class="overlay-icon">
                                            <i class="fa fa-btc" aria-hidden="true"></i>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>

                <div class="row">

                    <div class="col-12">
                        <div class="card mb-30">
                            <div class="card-body pb-0">
                                <h6 class="card-title mb-0">Recent Transactions(10)</h6>
                            </div>
                            <div class="card-body pb-0 px-0">
                                <div class="table-responsive table-borered">
                                    <table class="table table-nowrap table-analytics">
                                        <thead>
                                        <tr>
                                            <th>User Email</th>
                                            <th>Type</th>
                                            <th>Amount</th>
                                            <th>Status</th>
                                            <th>Method</th>
                                            <th>Wallet Address</th>
                                            <th>Date/time</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        @forelse($transactions as $transaction)

                                            <tr>
                                                <td>{{ $transaction->user->email }}</td>
                                                <td @class([
                                                     'text-success' => $transaction->type == 'deposit',
                                                     'text-danger' => $transaction->type == 'withdrawal',

                                                    ])>{{ ucfirst($transaction->type) }}</td>
                                                <td>${{ number_format($transaction->amount, 2) }}</td>
                                                <td><span @class([
                                                     'badge',
                                                     'badge-soft-warning' => $transaction->status == 'pending',
                                                     'badge-soft-success' => $transaction->status == 'confirmed',
                                                     'badge-soft-danger' => $transaction->status == 'failed',
                                                    ])>{{ ucfirst($transaction->status) }}</span></td>
                                                <td>{{ strtoupper($transaction->method) }}</td>
                                                <td>{{ $transaction->wallet_address }}</td>
                                                <td>{{ $transaction->created_at }}</td>
                                            </tr>
                                        @empty
                                        @endforelse

                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                        <!-- ./card -->
                    </div>
                </div>
            </div>
        </div>
        @endsection

@extends('layouts.admin')
@section('body')
    <div class="container-fluid mt-5">

        <div class="row">

            <div class="col-12">
                <div class="card mb-30">
                    <div class="card-body pb-0">
                        <h6 class="card-title mb-0">All Users</h6>
                    </div>
                    <div class="card-body pb-0 px-0">
                        <div class="table-responsive table-borered">
                            <table id="tab" class="table table-striped stripe row-border order-column table-nowrap table-analytics" cellspacing="3" width="100%">
                                <thead>
                                <tr>
                                    <th>S/N</th>
                                    <th>Name</th>
                                    <th>Email</th>
                                    <th>Address</th>
                                    <th>Country</th>
                                    <th>Balance</th>
                                    <th>Phone Number</th>
                                    <th>Status</th>
                                    <th>Registered</th>
                                    <th>Action</th>
                                </tr>
                                </thead>
                                <tbody>
                                @forelse($users as $key => $user)

                                    <tr>
                                        <td>{{$key}}</td>
                                        <td>{{ $user->name }}</td>
                                        <td>{{ $user->email }}</td>
                                        <td>{{ $user->address }}</td>
                                        <td>{{ $user->country }}</td>
                                        <td>{{ $user->balance }}</td>
                                        <td>{{ $user->phone_number }}</td>
                                        <td><span @class([
                                                     'badge',
                                                     'badge-soft-warning' => $user->verified == 'pending',
                                                     'badge-soft-success' => $user->verified == 'verified',
                                                     'badge-soft-danger' => $user->verified == 'failed',
                                                     'badge-soft-primary' => $user->verified == 'un-verified',
                                                    ])>{{ ucfirst($user->verified) }}</span></td>
                                        <td>{{ $user->created_at }}</td>
                                        <td>

                                            <form action="{{ route('admin.user-balance', ['id' => $user->id]) }}" method="get" style="display: inline;">
                                                @csrf
                                                <button type="submit" class="btn btn-primary inline">Balance</button>
                                            </form>
{{--                                            <button class="btn btn-success inline">Deposit</button>--}}

                                            <form action="{{ route('admin.user-delete', ['id' => $user->id]) }}" method="post" style="display: inline;">
                                                @csrf
                                                <button type="submit" class="btn btn-danger inline" >Delete</button>
                                            </form>

                                        </td>
                                    </tr>
                                @empty
                                @endforelse

                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
                <!-- ./card -->
            </div>
        </div>
    </div>

    <script type="text/javascript">
        $(document).ready(function() {
            var table = $('#tab').DataTable( );
        });
    </script>
@endsection

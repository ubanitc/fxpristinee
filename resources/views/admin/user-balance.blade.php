@extends('layouts.admin')
@section('body')
    @if(session('success'))
        <div class="alert bg-success">
            <span class="closebtn" onclick="this.parentElement.style.display='none';">×</span>
            <strong>Success - </strong> {{ session('success') }}
        </div>

    @endif

    <div class="col-12 box-margin height-card">

        <div class="card">
            <div class="card-body">
                <h6 class="card-title">User Balance</h6>
                <p>Update User  Balance</p>
                <form method="post" action="{{ route('admin.user-balance-update',['id'=>$user->id]) }}">
                    @csrf
                    <div class="row">
                        <div class="col-sm-4">
                            <div class="form-group">
                                <label class="control-label">Current Balance</label>
                                <div class="input-group">
                                    <input id="input1" type="text" value="{{ $user->balance }}" name="amount" placeholder="Enter Amount" class="form-control" aria-label="Enter Amount" aria-describedby="button-addon1">
                                </div>
                            </div>
                        </div><!-- Col -->
                    </div><!-- Row -->

                    <button type="submit" class="btn btn-success submit">Update Balance</button>

                </form>
            </div>
        </div>
    </div>
@endsection

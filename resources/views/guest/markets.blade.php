@extends('layouts.guest')
@section('body')
    <div class="uk-section uk-padding-remove-vertical in-equity-breadcrumb">
        <div class="uk-container">
            <div class="uk-grid">
                <div class="uk-width-1-1">
                    <ul class="uk-breadcrumb"><li><a href="{{ route('index') }}">Home</a></li><li><span>Markets</span></li></ul>
                </div>
            </div>
        </div>
    </div>
    <main>
        <!-- section content begin -->
        <div class="uk-section">
            <div class="uk-container">
                <div class="uk-grid-match uk-child-width-1-2@s uk-child-width-1-3@m in-card-10 uk-grid" data-uk-grid="">
                    <div class="uk-width-1-1 uk-first-column">
                        <h1 class="uk-margin-remove">A <span class="in-highlight">relationship</span> on your terms.</h1>
                        <p class="uk-text-lead uk-text-muted uk-margin-small-top">Work with us the way you want.</p>
                        <p>Some believe you must choose between an online broker and a wealth management firm. At our Company, you don’t need to compromise. Whether you invest on your own, with an advisor, or a little of both — we can support you.</p>
                    </div>
                    <div class="uk-grid-margin uk-first-column">
                        <div class="uk-card uk-card-default uk-card-body uk-box-shadow-small uk-border-rounded uk-light in-card-green">
                            <div class="in-icon-wrap uk-margin-bottom">
                                <i class="fas fa-seedling fa-lg"></i>
                            </div>
                            <h4 class="uk-margin-top">
                                <a href="#">Investing<i class="fas fa-chevron-right uk-float-right"></i></a>
                            </h4>
                            <hr>
                            <p>A wide selection of investment product to help build diversified portfolio</p>
                        </div>
                    </div>
                    <div class="uk-grid-margin">
                        <div class="uk-card uk-card-default uk-card-body uk-box-shadow-small uk-border-rounded uk-light in-card-blue">
                            <div class="in-icon-wrap uk-margin-bottom">
                                <i class="fas fa-chart-bar fa-lg"></i>
                            </div>
                            <h4 class="uk-margin-top">
                                <a href="#">Trading<i class="fas fa-chevron-right uk-float-right"></i></a>
                            </h4>
                            <hr>
                            <p>Powerful trading tools, resources, insight and support</p>
                        </div>
                    </div>
                    <div class="uk-grid-margin">
                        <div class="uk-card uk-card-default uk-card-body uk-box-shadow-small uk-border-rounded uk-light in-card-purple">
                            <div class="in-icon-wrap uk-margin-bottom">
                                <i class="fas fa-chart-pie fa-lg"></i>
                            </div>
                            <h4 class="uk-margin-top">
                                <a href="#">Wealth management<i class="fas fa-chevron-right uk-float-right"></i></a>
                            </h4>
                            <hr>
                            <p>Dedicated financial consultant to help reach your own specific goals</p>
                        </div>
                    </div>
                    <div class="uk-grid-margin uk-first-column">
                        <div class="uk-card uk-card-default uk-card-body uk-box-shadow-small uk-border-rounded uk-light in-card-navy">
                            <div class="in-icon-wrap uk-margin-bottom">
                                <i class="fas fa-chalkboard-teacher fa-lg"></i>
                            </div>
                            <h4 class="uk-margin-top">
                                <a href="#">Investment advisory<i class="fas fa-chevron-right uk-float-right"></i></a>
                            </h4>
                            <hr>
                            <p>A wide selection of investing strategies from seasoned portfolio managers</p>
                        </div>
                    </div>
                    <div class="uk-grid-margin">
                        <div class="uk-card uk-card-default uk-card-body uk-box-shadow-small uk-border-rounded uk-light in-card-grey">
                            <div class="in-icon-wrap uk-margin-bottom">
                                <i class="fas fa-funnel-dollar fa-lg"></i>
                            </div>
                            <h4 class="uk-margin-top">
                                <a href="#">Smart portfolio<i class="fas fa-chevron-right uk-float-right"></i></a>
                            </h4>
                            <hr>
                            <p>A revolutionary, fully-automated investmend advisory services</p>
                        </div>
                    </div>
                    <div class="uk-grid-margin">
                        <div class="uk-card uk-card-default uk-card-body uk-box-shadow-small uk-border-rounded uk-light in-card-orange">
                            <div class="in-icon-wrap uk-margin-bottom">
                                <i class="fas fa-handshake fa-lg"></i>
                            </div>
                            <h4 class="uk-margin-top">
                                <a href="#">Mutual fund advisor<i class="fas fa-chevron-right uk-float-right"></i></a>
                            </h4>
                            <hr>
                            <p>Specialized guidance from independent local advisor for hight-net-worth investors</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- section content end -->
        <!-- section content begin -->
        <div class="uk-section">
            <div class="uk-container">
                <div class="uk-grid">
                    <div class="uk-width-2-3@m">
                        <div class="uk-grid uk-grid-small" data-uk-grid="">
                            <div class="uk-width-auto@m uk-first-column">
                                <div class="in-icon-wrap large primary-color uk-margin-right">
                                    <i class="fas fa-money-bill-wave fa-2x"></i>
                                </div>
                            </div>
                            <div class="uk-width-expand">
                                <h3>Why trade with our Company?</h3>
                                <p>Experience the Future of Cryptocurrency Trading with {{ config('custom.app_name') }}! Our platform offers unparalleled security, ensuring your investments are always safe. Benefit from lightning-fast transactions and minimal fees, maximizing your returns. With an intuitive interface, both beginners and seasoned traders can navigate the market with ease. Our dedicated support team is always on hand, providing expert guidance and support. Join us at {{ config('custom.app_name') }} and unlock the potential of digital currency trading today!</p>
                                <div class="uk-grid uk-child-width-1-1 uk-child-width-1-2@m">
                                    <div>
                                        <ul class="uk-list in-list-check">
                                            <li>Direct Market Access (DMA)</li>
                                            <li>Leverage up to 1:500</li>
                                            <li>T+0 settlement</li>
                                            <li>Dividends paid in Crytpo</li>
                                            <li>Account managed by some of the best experts in the market</li>
                                        </ul>
                                    </div>
                                    <div class="in-margin-top-10@s in-margin-bottom-30@s">
                                        <ul class="uk-list in-list-check">
                                            <li>Free from UK Stamp Duty</li>
                                            <li>Short selling available</li>
                                            <li>Commissions from 0.08%</li>
                                            <li>Access to 1500 global shares</li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </div>
        <!-- section content end -->
        <!-- section content begin -->
        <div class="uk-section">
            <div class="uk-container">
                <div class="uk-grid">
                    <div class="uk-width-1-1 in-card-16">
                        <div class="uk-card uk-card-default uk-card-body uk-box-shadow-small uk-border-rounded">
                            <div class="uk-grid uk-flex-middle" data-uk-grid="">
                                <div class="uk-width-1-1 uk-width-expand@m uk-first-column">
                                    <h3>Get a free account Now!!!</h3>
                                </div>
                                <div class="uk-width-auto">
                                    <a class="uk-button uk-button-primary uk-border-rounded" href="{{ route('register') }}">Open an Account<i class="fas fa-arrow-circle-right uk-margin-small-left"></i></a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- section content end -->
    </main>
@endsection

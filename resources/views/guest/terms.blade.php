@extends('layouts.auth')
@section('body')
    <div class="container mt-5 text-white">

        <h2>Terms & Conditions</h2>

<p>This contract explains the use of various conditions applied to the services available at FxPristinee (henceforth “Our site”). For questions regarding these terms, please feel free to contact our customer support team. A direct link to Live Person can be found at the bottom left of the page. You may also contact us via e-mail at support@fxpristinee.com.</p>
<p>Our website is accessible worldwide to anyone with internet access. The access to and use of our site are subject to the following terms and conditions.</p>


<h5>BY USING AND ACCESSING OUR SITE, YOU ACCEPT, WITHOUT LIMITATION, ALL OF THESE TERMS AND CONDITIONS.</h5>


<p>We reserve the right to change these terms and conditions at any time. All changes or updates to the Terms & Conditions will be done so by a posting of a new and modified version on our site. By using our web site, you agree in advance that each use will be subject to the applicable Terms & Conditions. By using our web site, you accept its Terms & Conditions and Privacy Statement set forth below. If you do not agree with these policies, please discontinue using this site immediately.</p>

        <h4>1.    Online Services Agreement</h4>
<p>1.1.  This Agreement is made by and between FxPristinee (“Our site”) and you. This Agreement applies to both our site and trading platform, and to the electronic content and / or software currently contained on our site that provides the customers with real-time information on exchange rates of currencies, and the program transaction services on the forex market via internet, telephone or fax, and any other features, content or services that FxPristinee may add later (the “Services”).</p>


<h4>2.    Membership eligibility</h4>
<p>2.1.  Services are available and reserved only for individuals or businesses that can establish a legally binding contract under the laws applicable in their country of residence. Without limiting the undermentioned terms, our services are not available to people under the age of 18 or who have not attained the legal age (“Minors”). If you are a minor, you cannot use this service. PLEASE DO NOT USE THIS SITE IF YOU ARE NOT QUALIFIED.</p>
<p>2.2.  To avoid any doubt, we disclaim any liability for unauthorized use by minors of our services in any manner or another. In addition, our services are available only for people who have experience and sufficient knowledge in financial matters, and are able to evaluate the benefits and risks of acquiring financial contracts via this site. You are solely responsible for any decision made by you based on the content of our site.</p>
<p>2.3.  Without derogating from the above-mentioned provision, we disclaim any responsibility for auditing and/or checking your level of knowledge and experience, and any liability for damages or loss suffered as a direct result from your use of our site. This also applies to any transaction and/or use of our services. Without limiting the above-mentioned provisions, our services are not available in areas where their use is illegal. FxPristinee reserves the right to refuse and/or cancel access to its Services to anyone at its sole convenience.</p>


<h4>3.     Registration Information and Requirements.</h4>
<p>3.1.  When registering for a FxPristinee trade account, you will have to provide certain identifying information. You are responsible for the security of your account’s login and password with FxPristinee. You are solely responsible for any damage caused by reason of any act or omission resulting in improper or illegal use of your account.</p>
<p>3.2.  You agree to provide accurate and complete information about yourself during the registration process and must do your KYC (Know Your Customer) verification to allow you use the site fully. You also agree not to impersonate another person or entity, and not to hide your identity from FxPristinee for any reason whatsoever. If you register as a commercial entity, you declare that you have the required authority to bind that entity to this agreement. FxPristinee treats carefully the information you provide to us according to the disclosure of information provided during the registration process and privacy policy.</p>




<h4>4.    Legal restrictions</h4>
<p>4.1.  Without limiting the undermentioned provisions, you understand that laws regarding financial contracts vary throughout the world, and it is your responsibility to make sure you properly comply with any law, regulation or guideline in your country of residence regarding the use of our site. To avoid any doubt, the ability to access our site does not necessarily mean that our services and/or your activities through our site are legal under the laws, regulations or directives relevant to your country of residence. You hereby declare that the money in your FxPristinee account does not come from any illegal or criminal activity.</p>


<h4>5.    License limitations</h4>
<p>5.1.  FxPristinee  grants you a limited non-exclusive, non-transferable license to access and use our site (the “License”). The license is subject to your compliance with the terms of this agreement. You agree not to resell or make available our site to any other person, and will not copy any documents contained on our site for resale or for any other purpose without the prior written consent of FxPristinee.</p>

<p>5.2. To avoid any doubt, you will be liable and bound by any unauthorized use of our site, in violation of this section. You agree to use all the information received from the information systems FxPristinee for the sole purpose of performing transactions in the only limits of our site.</p>
<p>
    5.3.  You also agree not to use electronic communication feature of a service on our site for any illegal, abusive, intrusive, obscene, threatening or hateful purpose, as well as harassment and vilification in the privacy of others. The license granted hereunder shall terminate if FxPristinee considers that any information provided by you, including your e-mail is no longer current or accurate, or if you fail to comply with any term or condition of this agreement and all rules and guidelines for each service. It will be the same if FxPristinee trade determines that you committed a crime on the FxPristinee trading platform (including without limitation the conclusion of a transaction outside the market rates). If such a violation occurs, you agree to cease accessing the services. You agree that FxPristinee, in its sole discretion and with or without notice, may terminate your access to all or part of our services, close any open transaction and remove and discard any information or content within a service.</p>


<h4>6.      Risk Disclosure.</h4>
<p>6.1.  You agree to use our site at your own risk. Without limiting the undermentioned provisions, the services provided on this site is intended only to customers who are able to withstand the loss of any money they invest and who understand the risks and have experience in taking risks in financial markets. The possibility exists that you could endure a loss of some or all of your initial investment and hence you should not invest money that you cannot afford to lose. You should be aware of all the risks related to crypto, stock and binary options trading, and solicit advice from an independent financial advisor in case of doubts.</p>
<h4>7.    Market Data.</h4>
<p>7.1.  Through one or more of our services, FxPristinee can make available to you a wide range of financial information that is generated internally from agents, suppliers or partners (“Third Party Providers”). This includes, but is not limited to financial market data, quotes and news, analyst opinions and research reports, graphs and data (“Financial Information”).</p>
<p>7.2.  The financial information provided on this site is not intentional investment advice. FxPristinee offers financial information only as a service. FxPristinee and its third party providers do not warrant the accuracy, timeliness, completeness or correct sequencing of the financial information, or results of your use of this financial information. The financial information may promptly become unreliable for various reasons, including, for instance, changes in market conditions or economic circumstances. Neither FxPristinee nor the third party providers are required to update the information or opinions included in the financial information, and we can interrupt the flow of financial information at any time without notice.</p>
<p>7.3.  It is your responsibility to verify the reliability of the information on our site and its suitability for your needs. We exclude all liability for any claim, damage or loss of any kind caused by information contained in our site or referenced by our site.</p>


<h4>8.    Hyper Links.</h4>
<p>8.1.  FxPristinee  may offer a link to other websites that are provided or controlled by third parties. Such link to a site or sites is neither an endorsement or an approval nor a sponsorship or an affiliation to such site, its owners or its providers. FxPristinee trade recommends you make sure you understand the risks associated with the use of such sites before retrieving, using or purchasing via the Internet. Links to these sites are provided solely for your convenience and you agree not to hold FxPristinee responsible for any loss or damage due to the use or reliance on any content, products or services available on other sites.</p>


<h4>9.    Trading Cancellation.</h4>
<p>9.1.  FxPristinee reserves the right in its sole discretion, to refuse or cancel services, and/or refuse to distribute profits to any person for legitimate reasons, including, without limitation:</p>
<p>9.1.1.    If FxPristinee has reason to believe that a person’s activities on our site may be illegal.</p>
<p>9.1.2.    If FxPristinee may be harmed by any fiscal or pecuniary damage due to anyone’s activities.</p>
<p>9.1.3.    If FxPristinee considers that one or more operations on our site were made in violation of this agreement.</p>


<h4>10.  Payment procedure.</h4>
<p>10.1.  FxPristinee finance department supervises every withdrawal request submitted. Identification documents must be submitted for any withdrawal. There is no fee relating to withdrawal by credit card, but any withdrawal by wire transfer will be charged in the amount of $30.</p>
<p>10.2.  Once your trading session is over, a withdrawal request will be initiated automatically. A commission fee of 20% and a withdrawal fee ranging from 3% to 3.5% depending your country will be charged to you. Withdrawal may take up to 5 business days for it to be processed. Once processed, the funds will be available in your bank account. As to when the funds will be visible will depend on your bank.</p>
<p>10.3.  Depending on the currency to which your account is set, transactions made on www.FxPristinee.com will be represented on your billing statement as FxPristineee.</p>

<h4>11.  Bonuses.</h4>
<p>11.1.  All bonuses and promotions offered by FxPristinee are subject to several conditions, including but not limited to minimum deposit amounts, minimum trading volume and any other requirements detailed within the specific offer.</p>
<p>11.2.  In order to redeem a bonus, clients must meet FxPristinee required trading volume.</p>
<p>11.3.  Standard required trading volume necessary to redeem a bonus is 30 times the amount of the bonus.</p>
<p>11.4.  FxPristinee reserves the right to delay or withhold withdrawal of bonuses if any indication of deceptive or fraudulent activity, including but not limited to fraud, manipulation, cash back arbitrage, etc. based on the provisions of the bonus will result in a cancellation of all funds earned in the bonus and the termination of the client account.</p>
<p>11.5.If traders withdraw funds before reaching the required trading volume, the bonus and profits earned will become null and void, and will be deducted from the trader’s account.</p>
<p>11.6.Within the client's account, the funds that are deposited are kept separate from the bonuses received. All investments are first drawn from the deposit balance and only then, funds are drawn from the bonuses.</p>
<p>11.6.  FxPristinee offers the following Refer-A-Friend (RAF) bonus:</p>

        <ul>
            <li>11.6.1. Once a new client is referred to FxPristinee  by a current client, the current client must have an active account and have conducted at least $10,000 worth of trades on the FxPristinee site in order to be eligible.</li>
            <li>11.6.2. A client whose account has been blocked, closed or self-excluded from promotional offers at the time of the referral are not eligible for the RAF bonus. </li>
            <li>11.6.3. When a new client opens a FxPristinee  account and deposits a minimum of $100,000, both the referred and the referee will receive a bonus of 13%. </li>
            <li>11.6.4. This bonus can only be redeemed after both the old and the new clients have traded a minimum of 3times the bonus amount on the FxPristinee site. </li>
            <li>11.6.5. FxPristinee has the sole discretion to determine the allocation of supplementary bonuses and other benefits.</li>
            <li>11.6.6 The 13%  RAF bonus will be credited to the user accounts of the current and new client(s) immediately or a maximum of two business days after FxPristinee has approved the new client's membership and initial deposit, in accordance with the terms and conditions laid out in this section.</li>
            <li>11.7.  There is no limit as to how many new clients an active client may refer, but you cannot receive more than one RAF bonus for each new client referred to, and approved by FxPristinee.</li>
            <li>11.8.  FxPristinee will not accept any appeals initiated by the client or a third party regarding RAF bonuses. All decisions made by FxPristinee regarding the awarding of an RAF bonus, as well as any conditions related to the bonus, are final and non-negotiable.</li>
            <li>11.9.  All conditions stipulated in the Bonus super-section are applicable to the RAF Bonus as well.</li>
        </ul>

<h4>12.  Limited Liability.</h4>
<p>12.1.  We are committed to ensure continuity of the services on our site. However, we assume no responsibility for any error, omission, deletion, interruption, delay, defect, in operation or transmission, communications line failure, theft or destruction or unauthorized access or alteration of our site or services. We decline responsibility for any problems or technical malfunction of any telephone network or lines, computer online systems, servers or providers, hardware or software, or any technical failure because of technical problems or traffic congestion on the Internet, our site or any service.</p>
<p>12.2.  To the extent permitted by applicable law, in no event shall we be liable for any loss or damage arising from use of our site or services for any content posted on or through our site or services, or the conduct of all users of our site or services, whether online or offline.</p>


<p>12.3.  In no event FxPristinee or any of its directors, officers, employees, traders or account manager shall not be liable of any damages whatsoever to you including, without limitation, indirect, incidental, consequential or punitive damage arising out of or related to the use of our site or other chats on other verified social platforms as chats are directly monitored by the server and no call should or would be made except to our support.</p>
        <p>12.4.  LIMITATION OF LIABILITY SHALL APPLY TO THE FULLEST EXTENT PERMITTED BY LAW IN THE APPLICABLE JURISDICTION AND IN NO EVENT SHALL FxPristinee CUMULATIVE LIABILITY TO YOU EXCEED THE AMOUNT OF MONEY YOU TRANSFERRED OR DEPOSITED IN YOUR ACCOUNT ON OUR SITE IN CONNECTION WITH THE TRANSACTION GIVING RISE TO SUCH LIABILITY.</p>


<h4>13.  AML Procedures.</h4>
<p>13.1.  Prohibited uses: It is prohibited to abuse this site for purposes of money laundering. FxPristinee employs best practice anti-money laundering. FxPristinee reserves the right to refuse and to terminate any business relationship, and to cancel any operation of customers who do not comply with the requirements of anti-money laundering:</p>
        <ul>
            <li>13.1.1.  Online traders should provide all information required for registration.</li>
            <li>13.1.2.  The earnings will be paid to the person who first registered for an account online.</li>
            <li>13.1.2.  If your account is traded automatically, you are required to make payment for your fees mentioned above before withdrawal, please note this fee cannot be withdrawn from your balance.
            </li>
        </ul>
<p>13.2.  When a customer requests withdrawal through wire transfers, this will be processed to the sole owner of the originating bank account. When you make deposits in this way, it is the responsibility of your account manager to ensure that the clients account number and the registered name of the account owner accompany every transfer to FxPristinee.</p>
<p>13.3.  When a deposit is made using a credit card or debit card, profits will be distributed solely to the person whose name appears on the card used to make the deposit and will not be reimbursed on another card.</p>
<p>13.4.  Only one account is allowed per person. No gains can be levied on accounts opened under false names or multiple accounts opened by the same person.</p>
<p>13.5.  From time to time, FxPristinee may at its sole discretion, require from a customer to provide additional proof of identity such as a notarized copy of passport or other means of identity verification. FxPristinee may also, at its sole discretion, suspend an account until the required proof is provided.</p>



<h4>14.  Intellectual Property.</h4>
<p>14.1.  All content, including every trademark, service mark, trade name, logo and icon are the property of FxPristinee or its affiliates and are protected by law and international treaties and provisions relating to copyright. You agree not to remove copyright notices or other indications of protected intellectual property rights of any material you print or download from our site. You will not obtain intellectual property rights, or any right or license to use such material or our site, other than those set forth herein.</p>
<p>14.2.  Images displayed on our site are property of FxPristinee. You agree not to upload, post, distribute or reproduce any information, software or other material protected by copyright or other intellectual property right (including rights of publicity and privacy) without first obtaining permission from the copyright owner and the prior written consent of FxPristinee.</p>


<h4>15.  Indemnification.</h4>
<p>15.1.  You agree to defend and indemnify our company and its officers, directors, employees, and agents and to hold them harmless from and against any and all claims, liabilities, damages, losses, and expenses, including without limitation reasonable attorney’s fees and costs, arising out of / or in any way connected with your access to use of our site or services; your violation of any of the terms in this agreement; or your breach of any applicable laws or regulations. </p>


<h4>16.  Term and Termination.</h4>
<p>16.1.The term of the Agreement shall be unlimited however, our company will be allowed to terminate this agreement at any time by notice to you. As of termination, you shall not be able to carry out new transactions.</p>


<h4>17.  General Clause.</h4>
        <p>17.1.Our company (FxPristinee) will not be liable in any way to any persons in the event of force major, or for the act of any government or legal authority. This agreement shall be governed by and interpreted in accordance with the laws of London excluding that body of law pertaining to conflict of laws. Any legal action or proceeding arising under this agreement will be brought exclusively in courts located in London, and the parties hereby irrevocably consent to the personal jurisdiction and venue therein. In the event that any provision in this agreement is held to be invalid or unenforceable, the remaining provisions will remain in full force and effect. The failure of a party to enforce any right or provision of this agreement will not be deemed a waiver of such right or provision.</p>

<p>17.2.  Our company may assign this Agreement or any rights and/or obligations without your consent. Our company may amend the terms of this Agreement from time to time by posting the amended terms on our site. You are responsible for checking whether the agreement was amended. Any amendment shall come into force as of the day it was published on our site. If you do not agree to be bound by the changes to the terms and conditions of this agreement, do not use or access our services, and inform us in writing immediately.</p>
    </div>
@endsection

@extends('layouts.guest')
@section('body')
<!-- header end -->
<!-- breadcrumb content begin -->
<div class="uk-section uk-padding-remove-vertical in-equity-breadcrumb">
    <div class="uk-container">
        <div class="uk-grid">
            <div class="uk-width-1-1">
                <ul class="uk-breadcrumb"><li><a href="{{ route('index') }}">Home</a></li><li><span>About</span></li></ul>
            </div>
        </div>
    </div>
</div>
<!-- breadcrumb content end -->
<main>
    <!-- section content begin -->
    <div class="uk-section">
        <div class="uk-container">
            <div class="uk-grid">
                <div class="uk-width-1-1 uk-flex uk-flex-center">
                    <div class="uk-width-3-5@m uk-text-center">
                        <h1 class="uk-margin-remove-bottom">Putting our clients first <span class="in-highlight">since 1986</span></h1>
                        <p class="uk-text-lead uk-text-muted uk-margin-small-top">For more than 30 years, we’ve been empowering clients by helping them take control of their financial lives.</p>
                    </div>
                </div>
                <div class="uk-grid uk-grid-large uk-child-width-1-2@m uk-margin-medium-top row" data-uk-grid="">
                    <div class="uk-flex uk-flex-left uk-first-column">
                        <div class="uk-margin-right">
                            <div class="in-icon-wrap primary-color">
                                <i class="fas fa-leaf fa-lg"></i>
                            </div>
                        </div>
                        <div>
                            <h3>Philosophy</h3>
                            <p class="uk-margin-remove-bottom">Our philosophy is rooted in the belief that cryptocurrency is more than just a financial instrument; it's a revolutionary technology that empowers individuals. We are committed to providing a platform that is secure, user-friendly, and accessible to traders of all levels.</p>
                        </div>
                    </div>
                    <div class="uk-flex uk-flex-left">
                        <div class="uk-margin-right">
                            <div class="in-icon-wrap primary-color">
                                <i class="fas fa-hourglass-end fa-lg"></i>
                            </div>
                        </div>
                        <div>
                            <h3>History</h3>
                            <p class="uk-margin-remove-bottom">Since our inception, we have been at the forefront of the cryptocurrency revolution. Our journey began with a vision to simplify cryptocurrency trading, and over the years, we've evolved with the changing landscape, continuously enhancing our services.</p>
                        </div>
                    </div>
                </div>

                <div class="uk-grid uk-grid-large uk-child-width-1-2@m uk-margin-medium-top row" data-uk-grid="">
                    <div class="uk-flex uk-flex-left">
                        <div class="uk-margin-right">
                            <div class="in-icon-wrap primary-color">
                                <i class="fas fa-star fa-lg"></i>
                            </div>
                        </div>
                        <div>
                            <h3>Partnership</h3>
                            <p class="uk-margin-remove-bottom">After multiple awards and recognition received in the crypto currency industry, we have are in direct partnership with  Binance which is one of the biggest cryptocurrency exchange in the world, Streak and the WhiteHouse on different projects. Hereby, strengthening our community and creating top notch cryptocurrency experience and user friendly interface to our users and countless opportunities which are still ongoing and in creation, that will be strictly open to our users as benefits.</p>
                        </div>
                    </div>
                    <div class="uk-flex uk-flex-left">
                        <div class="uk-margin-right">
                            <div class="in-icon-wrap primary-color">
                                <i class="fas fa-flag fa-lg"></i>
                            </div>
                        </div>
                        <div>
                            <h3>Culture</h3>
                            <p class="uk-margin-remove-bottom">Our culture is built on innovation, integrity, and a relentless pursuit of excellence. We believe in fostering an environment that encourages creative problem-solving and continuous learning, ensuring we always stay ahead in the dynamic world of crypto trading as we are under regulation by the BBB, FTC, IMF and SEC</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- section content end -->
    <!-- section content begin -->
{{--    <div class="uk-section">--}}
{{--        <div class="uk-container">--}}
{{--            <div class="uk-grid">--}}
{{--                <div class="uk-width-1-1">--}}
{{--                    <div class="uk-card uk-card-default uk-border-rounded uk-background-center uk-background-contain uk-background-image@m" style="background-image: url(&quot;img/blockit/in-team-background-1.png&quot;); background-position-y: calc(50% + 0px); will-change: background-position-y;" data-uk-parallax="bgy: -100">--}}
{{--                        <div class="uk-card-body">--}}
{{--                            <div class="uk-grid uk-flex uk-flex-center">--}}
{{--                                <div class="uk-width-3-4@m uk-text-center">--}}
{{--                                    <h2>Our Leaders</h2>--}}
{{--                                    <p>We are a group of passionate, independent thinkers who never stop exploring new ways to improve trading for the self-directed investor.</p>--}}
{{--                                </div>--}}
{{--                            </div>--}}
{{--                            <div class="uk-child-width-1-3@m uk-text-center uk-margin-small-bottom uk-grid" data-uk-grid="">--}}
{{--                                <div class="uk-first-column">--}}
{{--                                    <img src="{{ asset('guest/img/blockit/in-team-1.png') }}" alt="image-team" width="200" height="200">--}}
{{--                                    <h4 class="uk-margin-small-top uk-margin-remove-bottom">Cynthia Dixon</h4>--}}
{{--                                    <span class="uk-label uk-text-small uk-border-rounded uk-margin-small-top uk-margin-small-bottom">Chief Executive Officer</span>--}}
{{--                                    <p>Omnis voluptas assumenda est dolor repellendus autem debit officiis</p>--}}
{{--                                    <div>--}}
{{--                                        <a class="uk-link-muted" href="#"><i class="fab fa-facebook-f uk-margin-small-right"></i></a>--}}
{{--                                        <a class="uk-link-muted" href="#"><i class="fab fa-twitter uk-margin-small-right"></i></a>--}}
{{--                                        <a class="uk-link-muted" href="#"><i class="fab fa-linkedin-in"></i></a>--}}
{{--                                    </div>--}}
{{--                                </div>--}}
{{--                                <div>--}}
{{--                                    <img src="{{ asset('guest/img/blockit/in-team-4.png') }}" alt="image-team" width="200" height="200">--}}
{{--                                    <h4 class="uk-margin-small-top uk-margin-remove-bottom">Bryan Greene</h4>--}}
{{--                                    <span class="uk-label uk-text-small uk-border-rounded uk-margin-small-top uk-margin-small-bottom">Human Resources</span>--}}
{{--                                    <p>Omnis voluptas assumenda omnis dolor repellendus</p>--}}
{{--                                    <div>--}}
{{--                                        <a class="uk-link-muted" href="#"><i class="fab fa-facebook-f uk-margin-small-right"></i></a>--}}
{{--                                        <a class="uk-link-muted" href="#"><i class="fab fa-twitter uk-margin-small-right"></i></a>--}}
{{--                                        <a class="uk-link-muted" href="#"><i class="fab fa-linkedin-in"></i></a>--}}
{{--                                    </div>--}}
{{--                                </div>--}}
{{--                                <div>--}}
{{--                                    <img src="{{ asset('guest/img/blockit/in-team-2.png') }}" alt="image-team" width="200" height="200">--}}
{{--                                    <h4 class="uk-margin-small-top uk-margin-remove-bottom">Arthur Parker</h4>--}}
{{--                                    <span class="uk-label uk-text-small uk-border-rounded uk-margin-small-top uk-margin-small-bottom">Executive Assistant</span>--}}
{{--                                    <p>Omnis voluptas assumenda omnis dolor repellendus</p>--}}
{{--                                    <div>--}}
{{--                                        <a class="uk-link-muted" href="#"><i class="fab fa-facebook-f uk-margin-small-right"></i></a>--}}
{{--                                        <a class="uk-link-muted" href="#"><i class="fab fa-twitter uk-margin-small-right"></i></a>--}}
{{--                                        <a class="uk-link-muted" href="#"><i class="fab fa-linkedin-in"></i></a>--}}
{{--                                    </div>--}}
{{--                                </div>--}}
{{--                            </div>--}}
{{--                        </div>--}}
{{--                    </div>--}}
{{--                </div>--}}
{{--            </div>--}}
{{--        </div>--}}
{{--    </div>--}}
    <!-- section content end -->
    <!-- section content begin -->
    <div class="uk-section">
        <div class="uk-container">
            <div class="uk-grid uk-flex uk-flex-center">
                <div class="uk-width-3-4@m">
                    <div class="uk-grid uk-flex uk-flex-middle" data-uk-grid="">
                        <div class="uk-width-1-2@m uk-first-column">
                            <h4 class="uk-text-muted">Number speaks</h4>
                            <h1 class="uk-margin-medium-bottom">We always ready<br>for a challenge.</h1>
                        </div>
                        <div class="uk-width-1-2@m">
                            <div class="uk-margin-large uk-grid" data-uk-grid="">
                                <div class="uk-width-1-3@m uk-first-column">
                                    <h1 class="uk-text-primary uk-text-right@m">
                                        <span class="count" data-counter-end="1035">0</span>
                                    </h1>
                                    <hr class="uk-divider-small uk-text-right@m">
                                </div>
                                <div class="uk-width-expand@m">
                                    <h4>Successful projects</h4>
                                    <p>Our track record boasts over a thousand successful projects, reflecting our ability to adapt and excel in the ever-evolving crypto market. </p>
                                </div>
                            </div>
                            <div class="uk-margin-large uk-grid" data-uk-grid="">
                                <div class="uk-width-1-3@m uk-first-column">
                                    <h1 class="uk-text-primary uk-text-right@m">
                                        <span class="count" data-counter-end="248">0</span>
                                    </h1>
                                    <hr class="uk-divider-small uk-text-right@m">
                                </div>
                                <div class="uk-width-expand@m">
                                    <h4>Unique designs</h4>
                                    <p>Our unique, client-centric approach to trading platform design sets us apart, ensuring an unparalleled trading experience.</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- section content end -->
</main>
@endsection

@extends('layouts.guest')
@section('body')
    <div class="uk-section uk-padding-remove-vertical in-equity-breadcrumb">
        <div class="uk-container">
            <div class="uk-grid">
                <div class="uk-width-1-1">
                    <ul class="uk-breadcrumb"><li><a href="{{ route('index') }}">Home</a><li><span>Contact</span></li></ul>
                </div>
            </div>
        </div>
    </div>
    <main>
        <!-- section content begin -->
        <div class="uk-section">
            <div class="uk-container">
                <div class="uk-grid uk-flex uk-flex-center in-contact-6">
                    <div class="uk-width-1-1">
                        <iframe class="uk-width-1-1 uk-height-large uk-border-rounded" src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3313.1768297916333!2d151.0301092756589!3d-33.85933341856845!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x6b12bcbb7f744a3d%3A0xd8bbcd4739a003ab!2sHargrave%20Rd%2C%20Auburn%20NSW%202144%2C%20Australia!5e0!3m2!1sen!2sca!4v1704701571850!5m2!1sen!2sca">
                        </iframe>
                    </div>
                    <div class="uk-width-3-5@m">
                        <div class="uk-grid uk-child-width-1-3@m uk-margin-medium-top uk-text-center" data-uk-grid="">
                            <div class="uk-first-column">
                                <h5 class="uk-margin-remove-bottom"><i class="fas fa-map-marker-alt fa-sm uk-margin-small-right"></i>Address</h5>
                                <p class="uk-margin-small-top">80 Hargrave Road
                                    CHUWAR QLD 4306</p>
                                <p>Westburry Business Tower 2,
                                    Burj Khalifa Blvd,
                                    Business Bay, Dubai.</p>
                                <p>10 Market Street
                                    Camana Bay
                                    Cayman Islands KY19006</p>
                            </div>
                            <div>
                                <h5 class="uk-margin-remove-bottom"><i class="fas fa-envelope fa-sm uk-margin-small-right"></i>Email</h5>
                                <p class="uk-margin-small-top uk-margin-remove-bottom">{{ config('custom.app_email') }}</p>
                                <p class="uk-text-small uk-text-muted uk-text-uppercase uk-margin-remove-top">for public inquiries</p>
                            </div>
                            <div>
                                <h5 class="uk-margin-remove-bottom"><i class="fas fa-phone-alt fa-sm uk-margin-small-right"></i>Call</h5>
                                <p class="uk-margin-small-top uk-margin-remove-bottom">{{ config('custom.app_phone') }}</p>
                                <p class="uk-text-small uk-text-muted uk-text-uppercase uk-margin-remove-top">Mon - Fri, 9am - 5pm</p>
                            </div>
                        </div>
                        <hr class="uk-margin-medium">
                        <p class="uk-margin-remove-bottom uk-text-lead uk-text-muted uk-text-center">Have a questions?</p>
                        <h1 class="uk-margin-small-top uk-text-center">Let's <span class="in-highlight">get in touch</span></h1>
                        <form id="contact-form" class="uk-form uk-grid-small uk-margin-medium-top uk-grid" data-uk-grid="">
                            <div class="uk-width-1-2@s uk-inline uk-first-column">
                                <span class="uk-form-icon fas fa-user fa-sm"></span>
                                <input class="uk-input uk-border-rounded" id="name" name="name" type="text" placeholder="Full name">
                            </div>
                            <div class="uk-width-1-2@s uk-inline">
                                <span class="uk-form-icon fas fa-envelope fa-sm"></span>
                                <input class="uk-input uk-border-rounded" id="email" name="email" type="email" placeholder="Email address">
                            </div>
                            <div class="uk-width-1-1 uk-inline uk-grid-margin uk-first-column">
                                <span class="uk-form-icon fas fa-pen fa-sm"></span>
                                <input class="uk-input uk-border-rounded" id="subject" name="subject" type="text" placeholder="Subject">
                            </div>
                            <div class="uk-width-1-1 uk-grid-margin uk-first-column">
                                <textarea class="uk-textarea uk-border-rounded" id="message" name="message" rows="6" placeholder="Message"></textarea>
                            </div>
                            <div class="uk-width-1-1 uk-grid-margin uk-first-column">
                                <button class="uk-width-1-1 uk-button uk-button-primary uk-border-rounded" id="sendemail" type="submit" name="submit">Send Message</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
        <!-- section content end -->
    </main>
@endsection

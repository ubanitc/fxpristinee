@extends('layouts.auth')
@section('body')
    <div class="container mt-5 mb-5 text-white">
        <h3>Privacy Policy</h3>

        <p>By Using Our Website You Accept this Privacy Policy
            Please read this Privacy Policy carefully and ensure that you understand it. Your acceptance of this Privacy Policy is deemed given every time you use Our Site.
            We understand that your privacy is important to you and that you care about how your personal data is used. We respect and value the privacy of everyone who visits this website, www.zeniq.com or its equivalent version on Android, iOS or other operating system, kernel or software environment that we make available to you or the public from time to time (“Our Site”) and will only collect and use personal data in ways that are described here, and in a way that is consistent with our obligations and your rights under the law.</p>

        <ul>
            <li>Definitions and Interpretation
                In this Policy the following terms shall have the following meanings: 
                “We/Us/Our”
                means FxPristinee
                “Account”
                means an account required to access and/or use certain areas and features of Our Site;
                “Cookie”
                means a small text file placed on your computer or device by our Site when you visit certain parts of our Site and/or when you use certain features of Our Site. Details of the Cookies used by Our Site are set out below.</li>
            <li>What Does This Policy Cover?
                This Privacy Policy applies only to your use of Our Site, which is operated by us. Our Site may contain links to other websites. Please note that we have no control over how your data is collected, stored, or used by other websites and we advise you to check the privacy policies of any such websites before providing any data to them.</li>
            <li>What Is Personal Data?
                Personal data is, in simple terms, any information about you that enables you to be identified. Personal data covers obvious information such as your name and contact details, but it also covers less obvious information such as identification numbers, electronic location data, and other online identifiers.</li>

            <li>What Are My Rights?
                Under this Privacy Policy, you have the following rights, which we will always work to uphold:
            <ol>The right to be informed about Our collection and use of your personal data. This Privacy Policy should tell you everything you need to know, but you can always contact us to find out more or to ask any questions using the details given below.</ol>
            <ol>The right to access the personal data we hold about you.</ol>
            <ol>The right to have your personal data rectified if any of your personal data held by us is inaccurate or incomplete.</ol>
            <ol>The right to be forgotten, i.e., the right to ask us to delete or otherwise dispose of any of your personal data that we hold.</ol>
                <ol>The right to restrict (i.e. prevent) the processing of your personal data.</ol>
                <ol>The right to object to us using your personal data for a particular purpose or purposes.</ol>
                <ol>The right to withdraw consent. This means that, if we are relying on your consent as the legal basis for using your personal data, you are free to withdraw that consent at any time.</ol>
                <ol>The right to data portability. This means that, if you have provided personal data to us directly, we are using it with your consent or for the performance of a contract, and that data is processed using automated means, you can ask us for a copy of that personal data to re-use with another service or business in many cases.</ol>
                <ol>Rights relating to automated decision-making and profiling.
                    For more information about Our use of your personal data or exercising your rights as outlined above, please contact us using the details provided below.
                    It is important that your personal data is kept accurate and up-to-date. If any of the personal data we hold about you changes, please keep us informed as long as we have that data.</ol>
            </li>
        </ul>


       <p>Further information about your rights can also be obtained from the Information Commissioner’s Office or your local Citizens Advice Bureau.
           If you have any cause for complaint about Our use of your personal data, you have the right to lodge a complaint with our customer care team at support@fxpristinee.com. We would welcome the opportunity to resolve your concerns ourselves so please contact us using the details given below.</p>
        <ul>
            <li>How Do I Contact You?
                To contact us about anything to do with your personal data and data protection, including to make a subject access request, please send us an email to support@fxpristinee.com.</li>
            <li>Changes to this Privacy Policy
                We may change this Privacy Notice from time to time. This may be necessary, for example, if the law changes, or if we change Our business in a way that affects personal data protection.
                Any changes will be immediately posted on Our Site and you will be deemed to have accepted the terms of the Privacy Policy on your first use of Our Site following the alterations. We recommend that you check this page regularly to keep up-to-date.</li>
        </ul>
    </div>
@endsection

@extends('layouts.guest')
@section('body')
<div class="uk-container" style="margin-top: 20px">
    <iframe width="100%" scrolling="yes" allowtransparency="true" class="mt-4" frameborder="0" src="https://cryptopanic.com/widgets/news/?bg_color=F5F7F9&amp;font_family=sans&amp;header_bg_color=30343B&amp;header_text_color=FFFFFF&amp;link_color=0091C2&amp;news_feed=recent&amp;posts_limit=10&amp;text_color=333333&amp;title=Latest%20News" height="700px"></iframe>
</div>
@endsection

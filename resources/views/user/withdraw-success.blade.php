@extends('layouts.auth')

@section('body')
    <div class="container settings text-white mt-5">
        <div class="card">
            <div class="card-body" style="display: flex; justify-content: center !important; flex-direction: column; align-items: center !important;">


                <script src="https://unpkg.com/@dotlottie/player-component@latest/dist/dotlottie-player.mjs" type="module"></script>

                <dotlottie-player src="https://lottie.host/cdd32226-5088-4321-9688-de48e0a72712/KfD53Wldjl.json" background="transparent" speed="1" style="width: 300px; height: 300px;" autoplay></dotlottie-player>



                <h5 style="text-align: center">Withdrawal Successful <br> Your withdrawal request is now Pending <br> <span style="text-decoration: underline"><a href="{{ route('dashboard') }}">Return to Dashboard</a></span> </h5>
            </div>
        </div>
    </div>

@endsection

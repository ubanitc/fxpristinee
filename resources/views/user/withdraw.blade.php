@extends('layouts.auth')

@section('body')
    <div class="container settings text-white mt-5">
        <div class="card">
            <div class="card-body">
                <h5 class="card-title">Withdrawal Details</h5>
                <h4>Withdraw {{ $request->amount }} USD</h4>
                <div class="row wallet-address">
                    <div class="col-md-8">
                        <form action="{{ route('withdraw.success') }}" method="post">
                            @csrf
                            <input type="hidden" name="amount" value="{{ $request->amount }}">
                        <p>Please input your withdrawal details below.</p>
{{--                        <p>After a successful deposit, you can close this page and return to your dashboard to see your updated balance.</p>--}}

                            @switch($request->currency)
                                @case('crypto')
                                    <div class="form-group">
                                        <label for="currency">Currency</label>
                                        <select name="currency" class="form-control" id="currency">
                                            <option value="btc">Bitcoin(BTC)</option>
                                            <option value="eth">Ethereum(ETH)</option>
                                            <option value="erc20/usdt">Tether(USDT|ERC20)</option>
                                        </select>

                                        {{--                            <div class="input-group-prepend">--}}
                                        {{--                                <button class="btn btn-primary" onclick="navigator.clipboard.writeText(document.getElementById('address').value)">COPY</button>--}}
                                        {{--                            </div>--}}
                                    </div>

                                    <div class="form-group">
                                        <label for="address">Wallet Address</label>
                                        <input type="text" id="address" name="address" required class="form-control" placeholder="Enter your address">
                                    </div>
                                @break

                                @case('paypal')
                                    <input type="hidden" name="currency" value="paypal">
                                    <div class="form-group">
                                        <label for="address">Paypal Email Address</label>
                                        <input type="email" id="address" name="address" required class="form-control" placeholder="Enter your email address">
                                    </div>
                                @break
                                @case('bank_transfer')
                                    <input type="hidden" name="currency" value="bank transfer">

                                    <div class="form-group">
                                        <label for="bank_name">Bank Name</label>
                                        <input type="text" id="bank_name" name="bank_name" required class="form-control" placeholder="Enter Bank Name">
                                    </div>
                                    <div class="form-group">
                                        <label for="bank_address">Bank Address</label>
                                        <input type="text" id="bank_address" class="form-control" name="bank_address" placeholder="Enter Bank Address">
                                    </div>

                                    <div class="form-group">
                                        <label for="sort_code">Sort Code</label>
                                        <input type="text" id="sort_code" class="form-control" name="sort_code" placeholder="Enter Bank Sort Code">
                                    </div>
                                    <div class="form-group">
                                        <label for="account_number">Account Number</label>
                                        <input type="text" id="account_number" class="form-control" required name="account_number" placeholder="Enter your Account Number">
                                    </div>
                                    <div class="form-group">
                                        <label for="routing_number">Routing Number</label>
                                        <input type="text" id="routing_number" class="form-control" name="routing_number" placeholder="Enter your Routing Number">
                                    </div>

                                @break
                            @endswitch

                            <button type="submit" class="btn btn-success">Withdraw</button>

                        </form>
                    </div>

                </div>
            </div>
        </div>
    </div>

@endsection

@extends('layouts.auth')

@section('body')
    <div class="container settings text-white mt-5">
        <div class="card">
            <div class="card-body">
                <h5 class="card-title">Wallet Deposit Address</h5>
                <div class="row wallet-address">
                    <div class="col-md-8">
                        <p>Payment an exact amount of {{$coin_exact_value}} {{ strtoupper($currency) }}. Please make payment to complete Deposit.</p>
                        <p>After a successful deposit, you can close this page and return to your dashboard to see your updated balance.</p>
                        <div class="input-group">
                            <input type="text" id="address" class="form-control" readonly value="{{ $payment_address }}">
                            <div class="input-group-prepend">
                                <button class="btn btn-primary" onclick="navigator.clipboard.writeText(document.getElementById('address').value)">COPY</button>
                            </div>
                        </div>
                        <h4 class="mt-5">Deposit Amount: {{$coin_exact_value}} {{ strtoupper($currency) }}</h4>

                    </div>
                    <div class="col-md-4">
                        <img src="data:image/jpeg;base64,{{ $qr_code->qr_code }}" alt="qr-code">
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection

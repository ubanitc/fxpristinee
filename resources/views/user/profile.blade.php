@extends('layouts.auth')
@section('body')
    <div class="settings mtb15">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-12 col-lg-3">
                    <div class="nav flex-column nav-pills settings-nav" id="v-pills-tab" role="tablist"
                         aria-orientation="vertical">
                        <a class="nav-link active" id="settings-profile-tab" data-toggle="pill" href="#settings-profile" role="tab"
                           aria-controls="settings-profile" aria-selected="false"><i class="icon ion-md-person"></i> Profile</a>
                        <a class="nav-link" id="settings-wallet-tab" data-toggle="pill" href="#settings-wallet" role="tab"
                           aria-controls="settings-wallet" aria-selected="false"><i class="icon ion-md-wallet"></i> Wallet</a>
                        <a class="nav-link" id="settings-tab" data-toggle="pill" href="#settings" role="tab"
                           aria-controls="settings" aria-selected="false"><i class="icon ion-md-settings"></i> Settings</a>
                    </div>
                </div>
                <div class="col-md-12 col-lg-9">
                    <div class="tab-content" id="v-pills-tabContent">
                        <div class="tab-pane fade show active" id="settings-profile" role="tabpanel"
                             aria-labelledby="settings-profile-tab">
                            <div class="card">
                                <div class="card-body">
                                    <h5 class="card-title">General Information</h5>
                                    <p class="text-success">{{ session('success') }}</p>
                                    <p class="text-info">{{ session('info') }}</p>

                                    <div class="settings-profile">
                                        <form action="{{ route('update.profile') }}" method="post" enctype="multipart/form-data">
                                            @csrf
                                            @method('PATCH')
                                            @if (Auth::user()->avatar)
                                                <img src="{{ asset('storage/' . Auth::user()->avatar) }}" width="64" height="64" alt="avatar" />
                                            @else
                                                <img src="{{ asset('assets/img/avatar.svg') }}" alt="avatar">
                                            @endif                                            <div class="custom-file">
                                                <input type="file" class="custom-file-input" name="avatar" id="fileUpload" />
                                                <label class="custom-file-label" for="fileUpload">Choose avatar</label>
                                            </div>
                                            <div class="form-row mt-4">
                                                <div class="col-md-6">
                                                    <label for="name">Full name</label>
                                                    <input id="name" type="text" name="name" value="{{ ucwords(Auth::user()->name) }}" class="form-control" placeholder="Full name">
                                                </div>
                                                <div class="col-md-6">
                                                    <label for="country">Country</label>
                                                    <input id="country" type="text" name="country" value="{{Auth::user()->country}}" class="form-control" placeholder="Country">
                                                </div>
                                                <div class="col-md-6">
                                                    <label for="email">Email Address</label>
                                                    <input id="email" type="text" class="form-control" name="email" value="{{Auth::user()->email}}" disabled placeholder="Enter your email">
                                                </div>
                                                <div class="col-md-6">
                                                    <label for="phone_number">Phone</label>
                                                    <input id="phone_number" type="text" name="phone_number" value="{{Auth::user()->phone_number}}" class="form-control" placeholder="Enter phone number">
                                                </div>
                                                <div class="col-md-6">
                                                    <label for="address">House Address</label>
                                                    <input id="address" type="text" name="address" class="form-control" value="{{Auth::user()->address}}" placeholder="Enter House Address">
                                                </div>
                                                <div class="col-md-12">
                                                    <input type="submit" value="Update">
                                                </div>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                            <div class="card">
                                <div class="card-body">
                                    <h5 class="card-title">Security Information</h5>
                                    @if ($errors->any())
                                        <div class="alert alert-danger">
                                            <ul>
                                                @foreach ($errors->all() as $error)
                                                    <li>{{ $error }}</li>
                                                @endforeach
                                            </ul>
                                        </div>
                                    @endif
                                    <p class="text-success">{{ session('password_success') }}</p>
                                    <div class="settings-profile">
                                        <form action="{{ route('update.profile.password') }}" method="post">
                                            @csrf
                                            @method('PATCH')
                                            <div class="form-row">
                                                <div class="col-md-6">
                                                    <label for="currentPass">Current password</label>
                                                    <input id="currentPass" type="text" class="form-control" name="current_password" placeholder="Enter your current password"
                                                           required />
                                                </div>
                                                <div class="col-md-6">
                                                    <label for="newPass">New password</label>
                                                    <input id="newPass" type="text" class="form-control" name="new_password" required placeholder="Enter new password">
                                                </div>
                                                <div class="col-md-6">
                                                    <label for="password_confirm">Password Confirm</label>
                                                    <input id="password_confirm" type="text" required class="form-control" name="new_password_confirmation" placeholder="Confirm new password">
                                                </div>

                                                <div class="col-md-12">
                                                    <input type="submit" value="Update">
                                                </div>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="tab-pane fade" id="settings-wallet" role="tabpanel" aria-labelledby="settings-wallet-tab">
                            <div class="wallet">
                                <div class="row">
                                    <div class="col-md-12 col-lg-4">
                                        <div class="nav flex-column nav-pills" role="tablist" aria-orientation="vertical">
                                            <a class="nav-link d-flex justify-content-between align-items-center active" data-toggle="pill"
                                               href="#coinBTC" role="tab" aria-selected="true">
                                                <div class="d-flex">
                                                    <img src="{{ asset('assets/img/icon/dollar.png') }}" alt="btc">
                                                    <div>
                                                        <h2>USD</h2>
                                                        <p>Dollars</p>
                                                    </div>
                                                </div>
                                                <div>
                                                    <h3>{{ Auth::user()->balance }}</h3>
                                                    <p class="text-right"><i class="icon ion-md-lock"></i> 0.0000000</p>
                                                </div>
                                            </a>
{{--                                            <a class="nav-link d-flex justify-content-between align-items-center" data-toggle="pill"--}}
{{--                                               href="#coinETH" role="tab" aria-selected="true">--}}
{{--                                                <div class="d-flex">--}}
{{--                                                    <img src="../assets/img/icon/1.png" alt="btc">--}}
{{--                                                    <div>--}}
{{--                                                        <h2>ETH</h2>--}}
{{--                                                        <p>Ethereum</p>--}}
{{--                                                    </div>--}}
{{--                                                </div>--}}
{{--                                                <div>--}}
{{--                                                    <h3>13.454845</h3>--}}
{{--                                                    <p class="text-right"><i class="icon ion-md-lock"></i> 0.0000000</p>--}}
{{--                                                </div>--}}
{{--                                            </a>--}}
{{--                                            <a class="nav-link d-flex justify-content-between align-items-center" data-toggle="pill"--}}
{{--                                               href="#coinBNB" role="tab" aria-selected="true">--}}
{{--                                                <div class="d-flex">--}}
{{--                                                    <img src="../assets/img/icon/9.png" alt="btc">--}}
{{--                                                    <div>--}}
{{--                                                        <h2>USDT</h2>--}}
{{--                                                        <p>Tether USDT</p>--}}
{{--                                                    </div>--}}
{{--                                                </div>--}}
{{--                                                <div>--}}
{{--                                                    <h3>35.4842458</h3>--}}
{{--                                                    <p class="text-right"><i class="icon ion-md-lock"></i> 0.0000000</p>--}}
{{--                                                </div>--}}
{{--                                            </a>--}}
                                        </div>
                                    </div>
                                    <div class="col-md-12 col-lg-8">
                                        <div class="tab-content">
                                            <div class="tab-pane fade show active" id="coinBTC" role="tabpanel">
                                                <div class="card">
                                                    <div class="card-body">
                                                        <h5 class="card-title">Balances</h5>
                                                        <ul>
                                                            <li class="d-flex justify-content-between align-items-center">
                                                                <div class="d-flex align-items-center">
                                                                    <i class="icon ion-md-cash"></i>
                                                                    <h2>Total Equity</h2>
                                                                </div>
                                                                <div>
                                                                    <h3>{{Auth::user()->balance}} USD</h3>
                                                                </div>
                                                            </li>
{{--                                                            <li class="d-flex justify-content-between align-items-center">--}}
{{--                                                                <div class="d-flex align-items-center">--}}
{{--                                                                    <i class="icon ion-md-checkmark"></i>--}}
{{--                                                                    <h2>Available Margin</h2>--}}
{{--                                                                </div>--}}
{{--                                                                <div>--}}
{{--                                                                    <h3>2.480 BTC</h3>--}}
{{--                                                                </div>--}}
{{--                                                            </li>--}}
                                                        </ul>
                                                        <button class="btn green" data-toggle="modal" data-target="#depositModal">Deposit</button>
                                                        <button class="btn red" data-toggle="modal" data-target="#withdrawModal">Withdraw</button>
                                                    </div>
                                                </div>
{{--                                                <div class="card">--}}
{{--                                                    <div class="card-body">--}}
{{--                                                        <h5 class="card-title">Wallet Deposit Address</h5>--}}
{{--                                                        <div class="row wallet-address">--}}
{{--                                                            <div class="col-md-8">--}}
{{--                                                                <p>Deposits to this address are unlimited. Note that you may not be able to withdraw all--}}
{{--                                                                    of your--}}
{{--                                                                    funds at once if you deposit more than your daily withdrawal limit.</p>--}}
{{--                                                                <div class="input-group">--}}
{{--                                                                    <input type="text" class="form-control" value="Ad87deD4gEe8dG57Ede4eEg5dREs4d5e8f4e">--}}
{{--                                                                    <div class="input-group-prepend">--}}
{{--                                                                        <button class="btn btn-primary">COPY</button>--}}
{{--                                                                    </div>--}}
{{--                                                                </div>--}}
{{--                                                            </div>--}}
{{--                                                            <div class="col-md-4">--}}
{{--                                                                <img src="../assets/img/qr-code-light.svg" alt="qr-code">--}}
{{--                                                            </div>--}}
{{--                                                        </div>--}}
{{--                                                    </div>--}}
{{--                                                </div>--}}
                                                <div class="card">
                                                    <div class="card-body">
                                                        <h5 class="card-title">Latest Transactions</h5>
                                                        <div class="wallet-history">
                                                            <table class="table">
                                                                <thead>
                                                                <tr>
                                                                    <th>No.</th>
                                                                    <th>Amount(USD)</th>
                                                                    <th>Method</th>
                                                                    <th>Status</th>
                                                                    <th>Date</th>
                                                                    <th>Type</th>
                                                                </tr>
                                                                </thead>
                                                                <tbody>
                                                                @forelse($transactions as $key => $transaction)
                                                                    <tr>
                                                                        <td>{{ $key }}</td>
                                                                        <td>{{ number_format($transaction->amount,2) }}</td>
                                                                        <td>{{ strtoupper($transaction->method)}}</td>
                                                                        <td><i @class([
                                                                            'icon' => true,
                                                                            'ion-md-checkmark-circle-outline green' => $transaction->status == 'confirmed',
                                                                            'ion-md-close-circle-outline red' => $transaction->status == 'failed',
                                                                            'ion-md-information-circle-outline yellow' => $transaction->status == 'pending',

                                                                            ]) class=" "></i></td>
                                                                        <td>{{ $transaction->created_at }}</td>
                                                                        <td>{{ucwords($transaction->type)}}</td>
                                                                    </tr>
                                                                @empty
                                                                    <tr>
                                                                        <td colspan="4">No transactions available.</td>
                                                                    </tr>
                                                                @endforelse

                                                                </tbody>
                                                            </table>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
{{--                                            <div class="tab-pane fade" id="coinETH" role="tabpanel">--}}
{{--                                                <div class="card">--}}
{{--                                                    <div class="card-body">--}}
{{--                                                        <h5 class="card-title">Balances</h5>--}}
{{--                                                        <ul>--}}
{{--                                                            <li class="d-flex justify-content-between align-items-center">--}}
{{--                                                                <div class="d-flex align-items-center">--}}
{{--                                                                    <i class="icon ion-md-cash"></i>--}}
{{--                                                                    <h2>Total Equity</h2>--}}
{{--                                                                </div>--}}
{{--                                                                <div>--}}
{{--                                                                    <h3>4.1542 ETH</h3>--}}
{{--                                                                </div>--}}
{{--                                                            </li>--}}
{{--                                                            <li class="d-flex justify-content-between align-items-center">--}}
{{--                                                                <div class="d-flex align-items-center">--}}
{{--                                                                    <i class="icon ion-md-checkmark"></i>--}}
{{--                                                                    <h2>Available Margin</h2>--}}
{{--                                                                </div>--}}
{{--                                                                <div>--}}
{{--                                                                    <h3>1.334 ETH</h3>--}}
{{--                                                                </div>--}}
{{--                                                            </li>--}}
{{--                                                        </ul>--}}
{{--                                                        <button class="btn green">Deposit</button>--}}
{{--                                                        <button class="btn red">Withdraw</button>--}}
{{--                                                    </div>--}}
{{--                                                </div>--}}
{{--                                                <div class="card">--}}
{{--                                                    <div class="card-body">--}}
{{--                                                        <h5 class="card-title">Wallet Deposit Address</h5>--}}
{{--                                                        <div class="row wallet-address">--}}
{{--                                                            <div class="col-md-8">--}}
{{--                                                                <p>Deposits to this address are unlimited. Note that you may not be able to withdraw all--}}
{{--                                                                    of your--}}
{{--                                                                    funds at once if you deposit more than your daily withdrawal limit.</p>--}}
{{--                                                                <div class="input-group">--}}
{{--                                                                    <input type="text" class="form-control" value="Ad87deD4gEe8dG57Ede4eEg5dREs4d5e8f4e">--}}
{{--                                                                    <div class="input-group-prepend">--}}
{{--                                                                        <button class="btn btn-primary">COPY</button>--}}
{{--                                                                    </div>--}}
{{--                                                                </div>--}}
{{--                                                            </div>--}}
{{--                                                            <div class="col-md-4">--}}
{{--                                                                <img src="../assets/img/qr-code-light.svg" alt="qr-code">--}}
{{--                                                            </div>--}}
{{--                                                        </div>--}}
{{--                                                    </div>--}}
{{--                                                </div>--}}
{{--                                                <div class="card">--}}
{{--                                                    <div class="card-body">--}}
{{--                                                        <h5 class="card-title">Latest Transactions</h5>--}}
{{--                                                        <div class="wallet-history">--}}
{{--                                                            <table class="table">--}}
{{--                                                                <thead>--}}
{{--                                                                <tr>--}}
{{--                                                                    <th>No.</th>--}}
{{--                                                                    <th>Date</th>--}}
{{--                                                                    <th>Status</th>--}}
{{--                                                                    <th>Amount</th>--}}
{{--                                                                </tr>--}}
{{--                                                                </thead>--}}
{{--                                                                <tbody>--}}
{{--                                                                <tr>--}}
{{--                                                                    <td>1</td>--}}
{{--                                                                    <td>25-04-2019</td>--}}
{{--                                                                    <td><i class="icon ion-md-checkmark-circle-outline green"></i></td>--}}
{{--                                                                    <td>4.5454334</td>--}}
{{--                                                                </tr>--}}
{{--                                                                <tr>--}}
{{--                                                                    <td>2</td>--}}
{{--                                                                    <td>25-05-2019</td>--}}
{{--                                                                    <td><i class="icon ion-md-checkmark-circle-outline green"></i></td>--}}
{{--                                                                    <td>0.5484468</td>--}}
{{--                                                                </tr>--}}
{{--                                                                <tr>--}}
{{--                                                                    <td>3</td>--}}
{{--                                                                    <td>25-06-2019</td>--}}
{{--                                                                    <td><i class="icon ion-md-close-circle-outline red"></i></td>--}}
{{--                                                                    <td>2.5454545</td>--}}
{{--                                                                </tr>--}}
{{--                                                                <tr>--}}
{{--                                                                    <td>4</td>--}}
{{--                                                                    <td>25-07-2019</td>--}}
{{--                                                                    <td><i class="icon ion-md-checkmark-circle-outline green"></i></td>--}}
{{--                                                                    <td>1.45894147</td>--}}
{{--                                                                </tr>--}}
{{--                                                                <tr>--}}
{{--                                                                    <td>3</td>--}}
{{--                                                                    <td>25-08-2019</td>--}}
{{--                                                                    <td><i class="icon ion-md-close-circle-outline red"></i></td>--}}
{{--                                                                    <td>2.5454545</td>--}}
{{--                                                                </tr>--}}
{{--                                                                </tbody>--}}
{{--                                                            </table>--}}
{{--                                                        </div>--}}
{{--                                                    </div>--}}
{{--                                                </div>--}}
{{--                                            </div>--}}
{{--                                            <div class="tab-pane fade" id="coinBNB" role="tabpanel">--}}
{{--                                                <div class="card">--}}
{{--                                                    <div class="card-body">--}}
{{--                                                        <h5 class="card-title">Balances</h5>--}}
{{--                                                        <ul>--}}
{{--                                                            <li class="d-flex justify-content-between align-items-center">--}}
{{--                                                                <div class="d-flex align-items-center">--}}
{{--                                                                    <i class="icon ion-md-cash"></i>--}}
{{--                                                                    <h2>Total Equity</h2>--}}
{{--                                                                </div>--}}
{{--                                                                <div>--}}
{{--                                                                    <h3>7.342 BNB</h3>--}}
{{--                                                                </div>--}}
{{--                                                            </li>--}}
{{--                                                            <li class="d-flex justify-content-between align-items-center">--}}
{{--                                                                <div class="d-flex align-items-center">--}}
{{--                                                                    <i class="icon ion-md-checkmark"></i>--}}
{{--                                                                    <h2>Available Margin</h2>--}}
{{--                                                                </div>--}}
{{--                                                                <div>--}}
{{--                                                                    <h3>0.332 BNB</h3>--}}
{{--                                                                </div>--}}
{{--                                                            </li>--}}
{{--                                                        </ul>--}}
{{--                                                        <button class="btn green">Deposit</button>--}}
{{--                                                        <button class="btn red">Withdraw</button>--}}
{{--                                                    </div>--}}
{{--                                                </div>--}}
{{--                                                <div class="card">--}}
{{--                                                    <div class="card-body">--}}
{{--                                                        <h5 class="card-title">Wallet Deposit Address</h5>--}}
{{--                                                        <div class="row wallet-address">--}}
{{--                                                            <div class="col-md-8">--}}
{{--                                                                <p>Deposits to this address are unlimited. Note that you may not be able to withdraw all--}}
{{--                                                                    of your--}}
{{--                                                                    funds at once if you deposit more than your daily withdrawal limit.</p>--}}
{{--                                                                <div class="input-group">--}}
{{--                                                                    <input type="text" class="form-control" value="Ad87deD4gEe8dG57Ede4eEg5dREs4d5e8f4e">--}}
{{--                                                                    <div class="input-group-prepend">--}}
{{--                                                                        <button class="btn btn-primary">COPY</button>--}}
{{--                                                                    </div>--}}
{{--                                                                </div>--}}
{{--                                                            </div>--}}
{{--                                                            <div class="col-md-4">--}}
{{--                                                                <img src="../assets/img/qr-code-light.svg" alt="qr-code">--}}
{{--                                                            </div>--}}
{{--                                                        </div>--}}
{{--                                                    </div>--}}
{{--                                                </div>--}}
{{--                                                <div class="card">--}}
{{--                                                    <div class="card-body">--}}
{{--                                                        <h5 class="card-title">Latest Transactions</h5>--}}
{{--                                                        <div class="wallet-history">--}}
{{--                                                            <table class="table">--}}
{{--                                                                <thead>--}}
{{--                                                                <tr>--}}
{{--                                                                    <th>No.</th>--}}
{{--                                                                    <th>Date</th>--}}
{{--                                                                    <th>Status</th>--}}
{{--                                                                    <th>Amount</th>--}}
{{--                                                                </tr>--}}
{{--                                                                </thead>--}}
{{--                                                                <tbody>--}}
{{--                                                                <tr>--}}
{{--                                                                    <td>1</td>--}}
{{--                                                                    <td>25-04-2019</td>--}}
{{--                                                                    <td><i class="icon ion-md-checkmark-circle-outline green"></i></td>--}}
{{--                                                                    <td>4.5454334</td>--}}
{{--                                                                </tr>--}}
{{--                                                                <tr>--}}
{{--                                                                    <td>2</td>--}}
{{--                                                                    <td>25-05-2019</td>--}}
{{--                                                                    <td><i class="icon ion-md-checkmark-circle-outline green"></i></td>--}}
{{--                                                                    <td>0.5484468</td>--}}
{{--                                                                </tr>--}}
{{--                                                                <tr>--}}
{{--                                                                    <td>3</td>--}}
{{--                                                                    <td>25-06-2019</td>--}}
{{--                                                                    <td><i class="icon ion-md-close-circle-outline red"></i></td>--}}
{{--                                                                    <td>2.5454545</td>--}}
{{--                                                                </tr>--}}
{{--                                                                <tr>--}}
{{--                                                                    <td>4</td>--}}
{{--                                                                    <td>25-07-2019</td>--}}
{{--                                                                    <td><i class="icon ion-md-checkmark-circle-outline green"></i></td>--}}
{{--                                                                    <td>1.45894147</td>--}}
{{--                                                                </tr>--}}
{{--                                                                <tr>--}}
{{--                                                                    <td>3</td>--}}
{{--                                                                    <td>25-08-2019</td>--}}
{{--                                                                    <td><i class="icon ion-md-close-circle-outline red"></i></td>--}}
{{--                                                                    <td>2.5454545</td>--}}
{{--                                                                </tr>--}}
{{--                                                                </tbody>--}}
{{--                                                            </table>--}}
{{--                                                        </div>--}}
{{--                                                    </div>--}}
{{--                                                </div>--}}
{{--                                            </div>--}}
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="tab-pane fade" id="settings" role="tabpanel" aria-labelledby="settings-tab">
                            <div class="card">
                                <div class="card-body">
                                    <h5 class="card-title">Notifications</h5>
                                    <div class="settings-notification">
                                        <ul>
                                            <li>
                                                <div class="notification-info">
                                                    <p>Update price</p>
                                                    <span>Get the update price in your dashboard</span>
                                                </div>
                                                <div class="custom-control custom-switch">
                                                    <input type="checkbox" class="custom-control-input" id="notification1" checked>
                                                    <label class="custom-control-label" for="notification1"></label>
                                                </div>
                                            </li>
                                            <li>
                                                <div class="notification-info">
                                                    <p>2FA</p>
                                                    <span>Unable two factor authentication service</span>
                                                </div>
                                                <div class="custom-control custom-switch">
                                                    <input type="checkbox" class="custom-control-input" id="notification2" >
                                                    <label class="custom-control-label" for="notification2"></label>
                                                </div>
                                            </li>
                                            <li>
                                                <div class="notification-info">
                                                    <p>Latest news</p>
                                                    <span>Get the latest news in your mail</span>
                                                </div>
                                                <div class="custom-control custom-switch">
                                                    <input type="checkbox" class="custom-control-input" id="notification3" checked>
                                                    <label class="custom-control-label" for="notification3"></label>
                                                </div>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
{{--                            <div class="card settings-profile">--}}
{{--                                <div class="card-body">--}}
{{--                                    <h5 class="card-title">Create API Key</h5>--}}
{{--                                    <div class="form-row">--}}
{{--                                        <div class="col-md-6">--}}
{{--                                            <label for="generateKey">Generate key name</label>--}}
{{--                                            <input id="generateKey" type="text" class="form-control" placeholder="Enter your key name">--}}
{{--                                        </div>--}}
{{--                                        <div class="col-md-6">--}}
{{--                                            <label for="rewritePassword">Confirm password</label>--}}
{{--                                            <input id="rewritePassword" type="password" class="form-control"--}}
{{--                                                   placeholder="Confirm your password">--}}
{{--                                        </div>--}}
{{--                                        <div class="col-md-12">--}}
{{--                                            <input type="submit" value="Create API key">--}}
{{--                                        </div>--}}
{{--                                    </div>--}}
{{--                                </div>--}}
{{--                            </div>--}}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Deposit Modal -->
    <div class="modal fade" id="depositModal" tabindex="-1" role="dialog" aria-labelledby="depositModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="depositModalLabel">Deposit Funds</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <form method="post" action="{{ route('deposit.paywall') }}">
                        @csrf
                        <div class="form-group">
                            <label for="depositMethod">Deposit Method</label>
                            <select name="currency" class="form-control" id="depositMethod">
                                <option value="btc">Bitcoin(BTC)</option>
                                <option value="eth">Ethereum(ETH)</option>
                                <option value="erc20/usdt">Tether(USDT|ERC20)</option>
                            </select>
                        </div>
                        <div class="form-group">
                            <label for="depositAmount">Amount</label>
                            <p>Minimum Deposit Amount: <b>{{number_format( $settings->min_deposit,2)}} USD</b></p>
                            <input type="number" class="form-control" name="amount" id="depositAmount" required min="{{ $settings->min_deposit}}" step="0.01" placeholder="Enter amount">
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
                            <button type="submit" class="btn btn-primary">Deposit</button>
                        </div>
                    </form>
                </div>

            </div>
        </div>
    </div>





{{--    Withdrawal modal--}}

    <div class="modal fade" id="withdrawModal" tabindex="-1" role="dialog" aria-labelledby="withdrawModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="withdrawModalLabel">Withdraw Funds</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <form method="post" action="{{ route('withdraw') }}">
                        @csrf
                        <div class="form-group">
                            <label for="withdrawMethod">Withdrawal Method</label>
                            <select name="currency" class="form-control" id="withdrawMethod">
                                <option value="crypto">Crypto</option>
                                <option value="bank_transfer">Bank Transfer</option>
                                <option value="paypal">PayPal</option>
                            </select>
                        </div>
{{--                        <div class="form-group">--}}
{{--                            <label for="withdrawAmount">Amount</label>--}}
{{--                            <p>Maximum Withdrawal Amount: <b>{{number_format( Auth::user()->balance,2)}} USD</b></p>--}}
                            <input type="hidden" class="form-control" name="amount" id="withdrawAmount" required value="{{ Auth::user()->balance}}" placeholder="Enter amount">
{{--                        </div>--}}
                        <div class="modal-footer">
                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
                            <button type="submit" class="btn btn-primary">Withdraw</button>
                        </div>
                    </form>
                </div>

            </div>
        </div>
    </div>
@endsection

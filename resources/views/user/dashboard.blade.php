@extends('layouts.auth')
@section('body')
    <style>
        .CryptoPanicWidget-header-logo{
            display: none;
        }
        .tradingview-widget-copyright{
            display: none !important;
        }
        .js-copyright-label{
            display: none !important;
        }
        .text-primaryText{
            display: none !important;
        }
    </style>
    @if(session('success'))
        <div class="alert alert-success alert-dismissible fade show" role="alert">
            <strong>{{ session('success') }}</strong>
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
        </div>
    @endif

<div class="container-fluid mtb15 no-fluid">
    <div class="row sm-gutters ">
        <div class="col-md-3 order-3 order-md-1">
            <iframe src=https://www.bitgetwidget.com/widget-tool/top-cryptocurrencies?theme=black&lang=en&showItems=corners&cols=symbol,price,vol_24h width='100%' height=1000 style="border: none"></iframe>
        </div>
        <div class="col-md-6  order-1 order-md-2">
            <div class="main-chart mb15 light-variant">
                <!-- TradingView Widget BEGIN -->
                <div class="tradingview-widget-container">
                    <div id="tradingview_e8053"></div>
                    <script src="{{ asset('assets/js/tv.js') }}"></script>
                    <script>
                        new TradingView.widget(
                            {
                                "width": "100%",
                                "height": 550,
                                "symbol": "BITSTAMP:BTCUSD",
                                "interval": "D",
                                "timezone": "Etc/UTC",
                                "theme": "Light",
                                "style": "1",
                                "locale": "en",
                                "toolbar_bg": "#f1f3f6",
                                "enable_publishing": false,
                                "withdateranges": true,
                                "hide_side_toolbar": false,
                                "allow_symbol_change": true,
                                "show_popup_button": true,
                                "popup_width": "1000",
                                "popup_height": "650",
                                "container_id": "tradingview_e8053"
                            }
                        );
                    </script>
                </div>
                <!-- TradingView Widget END -->
            </div>
            <div class="main-chart mb15 dark-variant">
                <!-- TradingView Widget BEGIN -->
                <div class="tradingview-widget-container">
                    <div id="tradingview_e8033"></div>
                    <script src="{{ asset('assets/js/tv.js') }}"></script>
                    <script>
                        new TradingView.widget(
                            {
                                "width": "100%",
                                "height": 550,
                                "symbol": "BITSTAMP:BTCUSD",
                                "interval": "D",
                                "timezone": "Etc/UTC",
                                "theme": "dark",
                                "style": "1",
                                "locale": "en",
                                "toolbar_bg": "#f1f3f6",
                                "enable_publishing": false,
                                "withdateranges": true,
                                "hide_side_toolbar": false,
                                "allow_symbol_change": true,
                                "show_popup_button": true,
                                "popup_width": "1000",
                                "popup_height": "650",
                                "container_id": "tradingview_e8033"
                            }
                        );
                    </script>
                </div>
                <!-- TradingView Widget END -->
            </div>
            <div class="market-trade">
                <ul class="nav nav-pills" role="tablist">
                    <li class="nav-item">
                        <a class="nav-link active" data-toggle="pill" href="#pills-trade-limit" role="tab"
                           aria-selected="true">Market Trade</a>
                    </li>
                </ul>
                <div class="tab-content">
                    <div class="tab-pane fade show active" id="pills-trade-limit" role="tabpanel">
                        <div class="d-flex justify-content-between">
                            <div class="market-trade-buy">
                                <form action="#">
                                    <div class="input-group">
                                        <input type="number" class="form-control" placeholder="Price" required />
                                        <div class="input-group-append">
                                            <span class="input-group-text">BTC</span>
                                        </div>
                                    </div>
                                    <div class="input-group">
                                        <input type="number" class="form-control" placeholder="Amount" required />
                                        <div class="input-group-append">
                                            <span class="input-group-text">ETH</span>
                                        </div>
                                    </div>
                                    <ul class="market-trade-list">
                                        <li><a href="#!">25%</a></li>
                                        <li><a href="#!">50%</a></li>
                                        <li><a href="#!">75%</a></li>
                                        <li><a href="#!">100%</a></li>
                                    </ul>
                                    <p>Available: <span> {{ Auth::user()->balance }} USD</span></p>
                                    <p>Volume: <span>0 BTC = 0 USD</span></p>
                                    <p>Margin: <span>{{ Auth::user()->balance }} USD</span></p>
                                    <p>Fee: <span>0 BTC = 0 USD</span></p>
                                    <button type="submit" class="btn buy">Buy</button>
                                </form>
                            </div>
                            <div class="market-trade-sell">
                                <form action="https://crypo-laravel-live.netlify.app/wallet">
                                    <div class="input-group">
                                        <input type="number" class="form-control" placeholder="Price" required />
                                        <div class="input-group-append">
                                            <span class="input-group-text">BTC</span>
                                        </div>
                                    </div>
                                    <div class="input-group">
                                        <input type="number" class="form-control" placeholder="Amount" required />
                                        <div class="input-group-append">
                                            <span class="input-group-text">ETH</span>
                                        </div>
                                    </div>
                                    <ul class="market-trade-list">
                                        <li><a href="#!">25%</a></li>
                                        <li><a href="#!">50%</a></li>
                                        <li><a href="#!">75%</a></li>
                                        <li><a href="#!">100%</a></li>
                                    </ul>
                                    <p>Available: <span>{{ Auth::user()->balance }} USD</span></p>
                                    <p>Volume: <span>0 BTC = 0 USD</span></p>
                                    <p>Margin: <span>{{ Auth::user()->balance }} USD</span></p>
                                    <p>Fee: <span>0 BTC = 0 USD</span></p>
                                    <button type="submit" class="btn sell">Sell</button>
                                </form>
                            </div>
                        </div>
                    </div>


                </div>
            </div>
        </div>
        <div class="col-md-3 order-2 order-md-3">
            <div class="order-book mb15">
                <h2 class="heading">Order Book</h2>
                <!-- TradingView Widget BEGIN -->
                <div class="tradingview-widget-container">
                    <div class="tradingview-widget-container__widget"></div>
                    <div class="tradingview-widget-copyright"><a href="https://www.tradingview.com/" rel="noopener nofollow" target="_blank"><span class="blue-text">Track all markets on TradingView</span></a></div>
                    <script type="text/javascript" src="https://s3.tradingview.com/external-embedding/embed-widget-hotlists.js" async>
                        {
                            "colorTheme": "dark",
                            "dateRange": "12M",
                            "exchange": "US",
                            "showChart": true,
                            "locale": "en",
                            "width": "100%",
                            "height": "600",
                            "largeChartUrl": "",
                            "isTransparent": true,
                            "showSymbolLogo": false,
                            "showFloatingTooltip": false,
                            "plotLineColorGrowing": "rgba(41, 98, 255, 1)",
                            "plotLineColorFalling": "rgba(41, 98, 255, 1)",
                            "gridLineColor": "rgba(240, 243, 250, 0)",
                            "scaleFontColor": "rgba(106, 109, 120, 1)",
                            "belowLineFillColorGrowing": "rgba(41, 98, 255, 0.12)",
                            "belowLineFillColorFalling": "rgba(41, 98, 255, 0.12)",
                            "belowLineFillColorGrowingBottom": "rgba(41, 98, 255, 0)",
                            "belowLineFillColorFallingBottom": "rgba(41, 98, 255, 0)",
                            "symbolActiveColor": "rgba(41, 98, 255, 0.12)"
                        }
                    </script>
                </div>
                <!-- TradingView Widget END -->
            </div>
            <div class="market-history">

                <iframe src=https://www.bitgetwidget.com/widget-tool/cryptocurrency-calculator?theme=black&lang=en&showItems=corners,title width='100%' id="widget-tanker" height=380 style="border: none"></iframe>
            </div>
        </div>
        <div class="col-md-3 order-5 order-md-4">
            <div class="market-news mt15">
                <h2 class="heading">Market News</h2>
                <iframe width="100%" scrolling="yes" allowtransparency="true" frameborder="0" src="https://cryptopanic.com/widgets/news/?bg_color=FFFFFF&amp;font_family=sans&amp;header_bg_color=30343B&amp;header_text_color=FFFFFF&amp;link_color=0091C2&amp;news_feed=recent&amp;text_color=333333&amp;title=Latest%20News" height="350px"></iframe>
            </div>
        </div>
        <div class="col-md-9 order-4 order-md-5">
            <div class="market-history market-order mt15">
                <ul class="nav nav-pills" role="tablist">
                    <li class="nav-item">
                        <a class="nav-link active" data-toggle="pill" href="#open-orders" role="tab" aria-selected="true">Open
                            Orders</a>
                    </li>
                </ul>
                <div class="tab-content">
                    <div class="tab-pane fade show active" id="open-orders" role="tabpanel">
                        <ul class="d-flex justify-content-between market-order-item">
                            <li>Time</li>
                            <li>All pairs</li>
                            <li>All Types</li>
                            <li>Buy/Sell</li>
                            <li>Price</li>
                            <li>Amount</li>
                            <li>Executed</li>
                            <li>Unexecuted</li>
                        </ul>
                        <span class="no-data">
                <i class="icon ion-md-document"></i>
                No data
              </span>
                    </div>
                    <div class="tab-pane fade" id="stop-orders" role="tabpanel">
                        <ul class="d-flex justify-content-between market-order-item">
                            <li>Time</li>
                            <li>All pairs</li>
                            <li>All Types</li>
                            <li>Buy/Sell</li>
                            <li>Price</li>
                            <li>Amount</li>
                            <li>Executed</li>
                            <li>Unexecuted</li>
                        </ul>
                        <span class="no-data">
                <i class="icon ion-md-document"></i>
                No data
              </span>
                    </div>
                    <div class="tab-pane fade" id="order-history" role="tabpanel">
                        <ul class="d-flex justify-content-between market-order-item">
                            <li>Time</li>
                            <li>All pairs</li>
                            <li>All Types</li>
                            <li>Buy/Sell</li>
                            <li>Price</li>
                            <li>Amount</li>
                            <li>Executed</li>
                            <li>Unexecuted</li>
                        </ul>
                        <span class="no-data">
                <i class="icon ion-md-document"></i>
                No data
              </span>
                    </div>
                    <div class="tab-pane fade" id="trade-history" role="tabpanel">
                        <ul class="d-flex justify-content-between market-order-item">
                            <li>Time</li>
                            <li>All pairs</li>
                            <li>All Types</li>
                            <li>Buy/Sell</li>
                            <li>Price</li>
                            <li>Amount</li>
                            <li>Executed</li>
                            <li>Unexecuted</li>
                        </ul>
                        <span class="no-data">
                <i class="icon ion-md-document"></i>
                No data
              </span>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

    <script>
        window.onload = function() {
            var iframe = document.getElementById('widget-tanker');
            var innerDoc = iframe.contentDocument || iframe.contentWindow.document;

            var elements = innerDoc.getElementsByClassName('text-primaryText');
            for(var i = 0; i < elements.length; i++) {
                elements[i].style.display = 'none';
            }}
    </script>

    @if(Auth::user()->verified == 'un-verified' || Auth::user()->verified == 'failed' )
        <!-- Modal -->
        <div class="modal fade" id="verificationModal" tabindex="-1" role="dialog" aria-labelledby="verificationModalLabel" aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="verificationModalLabel">Identity Verification</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <form method="post" action="{{ route('verification') }}" enctype="multipart/form-data">

                        <div class="modal-body">
                            <p>Please upload the required documents for account verification.</p>
                            @csrf
                            <div class="form-group">
                                <label for="type">Identification Type</label>
                                <select class="form-control" name="type" id="type">
                                    <option value="international_passport">International Passport</option>
                                    <option value="drivers_license">Drivers License</option>
                                    <option value="id_card">Identification Card (ID Card)</option>
                                </select>
                            </div>
                            <div class="form-group">
                                <label for="picture">Upload (Front)</label>
                                <input type="file" class="form-control-file" name="picture" id="picture">
                            </div>
                            <div class="form-group" id="backPictureGroup" style="display: none;">
                                <label for="backPicture">Upload (Back)</label>
                                <input type="file" class="form-control-file" name="back_picture" id="backPicture">
                            </div>
                            <div class="form-group">
                                <label for="addressInput">Your Address</label>
                                <input type="text" name="address" class="form-control" id="addressInput" placeholder="Enter your address">
                            </div>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
                            <button type="submit" class="btn btn-primary">Submit</button>
                        </div>
                    </form>




                </div>
            </div>
        </div>
        @push('custom-script')
            <script>
                $(document).ready(function(){
                    $("#verificationModal").modal('show');
                    $("#verify-popup").click(function (){
                        $("#verificationModal").modal('show');
                    });
                });

            </script>

            <script>
                document.getElementById('type').addEventListener('change', function () {
                    var backPictureGroup = document.getElementById('backPictureGroup');
                    if (this.value === 'international_passport') {
                        backPictureGroup.style.display = 'none';
                    } else {
                        backPictureGroup.style.display = 'block';
                    }
                });
            </script>
        @endpush
    @endif


@endsection


<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Setting;
use App\Models\Transaction;
use App\Models\User;
use Illuminate\Http\Request;

class DashboardController extends Controller
{
    public function index(){

        $user_count = User::count();
        $deposit_count = Transaction::where('type','deposit')->count();
        $pending_withdrawals = Transaction::where('type','withdrawal')->where('status','pending')->count();
        $pending_verifications = User::where('verified','pending')->count();
        $transactions = Transaction::orderBy('id','desc')->get()->take(5);

        return view('admin.index', compact('user_count', 'deposit_count','pending_verifications','pending_withdrawals', 'transactions'));
    }

    public function verification(){
        $verificationQuery = User::query();
        $pending = clone $verificationQuery; // Clone for the pending query
        $approved = clone $verificationQuery; // Clone for the approved query

        $pendingUsers = $pending->where('verified', 'pending')->get();
        $approvedUsers = $approved->where('verified', 'verified')->get();


        return view('admin.verification', compact('pendingUsers','approvedUsers'));
    }

    public function users(){
        $users = User::orderBy('id','desc')->get();
        return view('admin.users', compact('users'));
    }

    public function transactions(){
        $transactions = Transaction::orderBy('id','desc')->get();

        return view('admin.transactions', compact('transactions'));
    }

    public function settings(){
        $settings = Setting::first();

        return view('admin.settings', compact('settings'));
    }

    public function settings_save(Request $request){
        $setting = Setting::first();

        $setting->btc_address = $request->btc_address;
        $setting->eth_address = $request->eth_address;
        $setting->usdt_address = $request->usdt_address;
        $setting->min_deposit = $request->min_deposit;
        $setting->save();
        session()->flash('success','Setting Saved Successfully');
        return redirect()->back();
    }

    public function withdrawals(){

        $withdrawals = Transaction::where('type','withdrawal')->where('status','pending')->orderBy('id','desc')->get();

        return view('admin.withdrawals',compact('withdrawals'));
    }

    public function withdrawals_approve($id){
        $withdrawal = Transaction::find($id);

        $withdrawal->status = 'confirmed';
        $withdrawal->save();

        session()->flash('success','Approved Successfully');
        return redirect()->back();
    }


    public function withdrawals_decline($id){
        $withdrawal = Transaction::find($id);

        $withdrawal->status = 'failed';
        $withdrawal->save();

        session()->flash('failed','Declined Successfully');
        return redirect()->back();
    }

    public function verification_approve($id){
        $user = User::find($id);
        $user->verified = 'verified';
        $user->save();
        return redirect()->back();
    }

    public function verification_decline($id){
        $user = User::find($id);
        $user->verified = 'failed';
        $user->save();
        return redirect()->back();
    }

    public function user_balance($id){
        $user = User::find($id);
        return view('admin.user-balance', compact('user'));
    }
    public function user_balance_update(Request $request, $id){

        $user = User::find($id);
        $user->balance = $request->amount;
        $user->save();
        session()->flash('success',$user->names."'s Balance has Been updated");
        return redirect()->back();
    }

    public function user_delete($id){
        $user = User::find($id);
        if ($user) {
            // Delete related transactions
            foreach ($user->transactions as $transaction) {
                $transaction->delete();
            }

            // Delete the user
            $user->delete();
        }
        return redirect()->back();
    }

}

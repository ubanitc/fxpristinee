<?php

namespace App\Http\Controllers\User;

use App\Http\Controllers\Controller;
use App\Models\Setting;
use App\Models\Transaction;
use App\Models\User;
use CryptAPI\CryptAPI;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Crypt;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Validator;
use function Termwind\renderUsing;

class DashboardController extends Controller
{
    public function dashboard(){
        return view('user.dashboard');
    }
    public function profile()
    {
        $settings = Setting::first();
        $transactions = Transaction::where('user_id', Auth::id())->get();
        return view('user.profile', compact('settings','transactions'));
    }
    public function my_wallet()
    {
        $settings = Setting::first();
        $transactions = Transaction::where('user_id', Auth::id())->get();
        return view('user.my-wallet', compact('settings','transactions'));
    }

    public function settings()
    {
        $settings = Setting::first();
        $transactions = Transaction::where('user_id', Auth::id())->get();
        return view('user.settings', compact('settings','transactions'));
    }

    public function update_user_info(Request $request){
        $user = Auth::user();

        // Check if the avatar file is uploaded
        if ($request->hasFile('avatar')) {
            $file = $request->file('avatar');
            $filename = time() . '_' . $file->getClientOriginalName(); // Create a unique file name

            // Store the file in the storage/app/public/avatars directory
            $filePath = $file->storeAs('avatars', $filename, 'public');

            // Update the user's avatar path in the database
            $user->avatar = $filePath;
        }

        // Update the user's information only if it's changed
        $user->fill([
            'name' => $request->name,
            'country' => $request->country,
            'phone_number' => $request->phone_number,
            'address' => $request->address,
        ]);

        // Check if any of the fields are dirty (changed)
        if ($user->isDirty()) {
            $user->save();
            return back()->with('success', 'Profile updated successfully.');
        }

        return back()->with('info', 'No changes were made to your profile.');
    }

    public function profile_password_change(Request $request){
        $request->validate([
            'current_password' => 'required',
            'new_password' => 'required|string|min:8|confirmed',
        ]);

        $user = Auth::user();

        if (!Hash::check($request->current_password, $user->password)) {
            return back()->withErrors(['current_password' => 'Your current password does not match our records.']);
        }

        $user->password = Hash::make($request->new_password);
        $user->save();

        return back()->with('password_success', 'Password successfully updated.');
    }


    public function paywall(Request $request){

        $settings = Setting::first();
        $currency = $request->currency;
        $callback_url = config('custom.webhook_url');
        $parameters = [
            'user_id' => Auth::id()
        ];
        $crypt_params = [
            'pending' => 1,
            'email' => 'tochukwu.ubani@gmail.com',
            'post' => 1,
            'convert' => 1
        ];


        switch ($request->currency){
            case 'btc':
                $address = $settings->btc_address;
            break;
            case 'eth':
                $address = $settings->eth_address;
                break;
            case 'erc20/usdt':
                $address = $settings->usdt_address;
                $currency = 'erc20_usdt';
                break;
        }
        $ca = new CryptAPI($currency,$address,$callback_url,$parameters,$crypt_params);

        $payment_address = $ca->get_address();
        $coin_value = CryptAPI::get_convert($currency, $request->amount, 'USD');
        $coin_exact_value = $coin_value->value_coin;
        $qr_code = $ca->get_qrcode($coin_exact_value);

        return view('user.paywall', compact('payment_address', 'qr_code', 'coin_value','currency','coin_exact_value'));
    }

    public function payment_callback(Request $request){
        $payment_data = CryptAPI::process_callback($request->all());
        $amount = json_decode($payment_data['value_coin_convert'], true);
        $user = User::find($payment_data['user_id']);

        if ($payment_data['confirmations'] == '0'){
            $user->balance += $amount['USD'];
            $user->save();
            Transaction::create([
                'user_id' => $payment_data['user_id'],
                'method' => $payment_data['coin'],
                'amount' => $amount['USD'],
                'wallet_address' => $payment_data['address_in'],
                'status' => 'confirmed',
                'type' => 'deposit'
            ]);
        }


        Log::info('Webhook received:', $request->all() );

        return "*ok*";
    }

    public function withdraw(Request $request){

       return view('user.withdraw',compact('request'));
    }

    public function withdrawal_successful(Request $request){

        $bank = 'Bank Name: '.$request->bank_name." Bank Address: ". $request->bank_name." sortcode: ".$request->sort_code." account number: ".$request->account_number." Routing Number: ".$request->routing_number;
        $withdrawal = Transaction::create([
            'user_id' => Auth::id(),
            'method' => $request->currency,
            'amount' => $request->amount,
            'wallet_address' => $request->address ?? $bank,
            'status' => 'pending',
            'type' => 'withdrawal'
        ]);
        $user = Auth::user();

        $user->balance -= $request->amount;
        $user->save();

       return view('user.withdraw-success');
    }



    public function verification(Request $request){

        $request->validate( [
            'picture' => 'required',
            'back_picture' => 'required'
            ]);
        $user = Auth::user();
        if ($request->hasFile('picture')) {
            $file = $request->file('picture');
            $filename = time() . '_' . $file->getClientOriginalName(); // Create a unique file name

            // Store the file in the storage/app/public/avatars directory
            $filePath = $file->storeAs('ids', $filename, 'public');

            // Update the user's avatar path in the database
            $user->id_1 = $filePath;
        }

        if ($request->hasFile('back_picture')){
            $file = $request->file('back_picture');
            $filename = time() . '__' . $file->getClientOriginalName(); // Create a unique file name

            // Store the file in the storage/app/public/avatars directory
            $filePath = $file->storeAs('ids', $filename, 'public');

            // Update the user's avatar path in the database
            $user->id_2 = $filePath;
        }

            $user->id_type = $request->type;
            $user->address = $request->address;
            $user->verified = 'pending';

            $user->save();
            session()->flash('success','Your Upload was Successful and is Pending verification');
            return redirect()->back();

    }
}

<?php

return [

    'app_name' => env('APP_NAME', 'Laravel'),
    'app_email' => env('APP_EMAIL'),
    'app_phone' => env('APP_PHONE'),
    'app_address' => env('APP_ADDRESS'),
    'base_url' => env('APP_URL'),
    'webhook_url' => env('WEBHOOK_URL'),

];

<?php

use App\Http\Controllers\ProfileController;
use Illuminate\Support\Facades\Artisan;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "web" middleware group. Make something great!
|
*/

//Guest Routes
Route::get('/artisan', function (){
   Artisan::call('route:clear');
   Artisan::call('cache:clear');
   Artisan::call('view:clear');
   Artisan::call('optimize');
   Artisan::call('storage:link');
   return "calls  successful";
});
Route::get('/migrate-fresh', function (){
   Artisan::call('migrate:fresh');
});
Route::get('/migrate', function (){
   Artisan::call('/migrate');
});
Route::view('/', 'guest.index')->name('index');
Route::view('/about','guest.about')->name('about');
Route::view('/news','guest.news')->name('news');
Route::view('/contact','guest.contact')->name('contact');
Route::view('/markets','guest.markets')->name('markets');
Route::view('/terms-and-conditions','guest.terms')->name('terms');
Route::view('/privacy-policies', 'guest.privacy-policies')->name('privacy-policies');

//Callback URL
Route::post('/payment-callbacks', [\App\Http\Controllers\User\DashboardController::class,'payment_callback'])->name('payment-callback');


//User Routes
Route::group([
    'middleware' => ['auth','user'],

], function (){
    Route::get('/dashboard', [\App\Http\Controllers\User\DashboardController::class,'dashboard'])->name('dashboard');
    Route::get('/profile', [\App\Http\Controllers\User\DashboardController::class, 'profile'])->name('profile.view');
    Route::get('/my-wallet',[\App\Http\Controllers\User\DashboardController::class,'my_wallet'])->name('my-wallet');
    Route::get('/settings',[\App\Http\Controllers\User\DashboardController::class,'settings'])->name('settings');
    Route::patch('/update-profile', [\App\Http\Controllers\User\DashboardController::class, 'update_user_info'])->name('update.profile');
    Route::patch('/update-profile-password', [\App\Http\Controllers\User\DashboardController::class, 'profile_password_change'])->name('update.profile.password');
    Route::post('/deposit-paywall', [\App\Http\Controllers\User\DashboardController::class,'paywall'])->name('deposit.paywall');
    Route::post('/withdraw', [\App\Http\Controllers\User\DashboardController::class,'withdraw'])->name('withdraw');
    Route::post('/withdrawal-success',[\App\Http\Controllers\User\DashboardController::class,'withdrawal_successful'])->name('withdraw.success');
    Route::post('/verification',[\App\Http\Controllers\User\DashboardController::class,'verification'])->name('verification');
});


//Admin Routes
Route::group([
   'middleware' => ['auth','admin','verified'],
   'prefix' => 'admin',
   'as'=>'admin.'
], function (){
    Route::get('/dashboard', [\App\Http\Controllers\Admin\DashboardController::class,'index'])->name('dashboard');
    Route::get('/verification', [\App\Http\Controllers\Admin\DashboardController::class,'verification'])->name('verification');
    Route::get('/users', [\App\Http\Controllers\Admin\DashboardController::class,'users'])->name('users');
    Route::get('/transactions', [\App\Http\Controllers\Admin\DashboardController::class,'transactions'])->name('transactions');
    Route::get('/settings', [\App\Http\Controllers\Admin\DashboardController::class,'settings'])->name('settings');
    Route::post('/settings-save', [\App\Http\Controllers\Admin\DashboardController::class,'settings_save'])->name('settings-save');
    Route::get('/withdrawals', [\App\Http\Controllers\Admin\DashboardController::class,'withdrawals'])->name('withdrawals');
    Route::post('/withdrawals-approve/{id}', [\App\Http\Controllers\Admin\DashboardController::class,'withdrawals_approve'])->name('withdrawals-approve');
    Route::post('/withdrawals-decline/{id}', [\App\Http\Controllers\Admin\DashboardController::class,'withdrawals_decline'])->name('withdrawals-decline');
    Route::post('/verification-approve/{id}', [\App\Http\Controllers\Admin\DashboardController::class,'verification_approve'])->name('verification-approve');
    Route::post('/verification-decline/{id}', [\App\Http\Controllers\Admin\DashboardController::class,'verification_decline'])->name('verification-decline');
    Route::get('/user-balance/{id}', [\App\Http\Controllers\Admin\DashboardController::class,'user_balance'])->name('user-balance');
    Route::post('/user-balance-update/{id}', [\App\Http\Controllers\Admin\DashboardController::class,'user_balance_update'])->name('user-balance-update');
    Route::post('/user-delete/{id}', [\App\Http\Controllers\Admin\DashboardController::class,'user_delete'])->name('user-delete');
});

require __DIR__.'/auth.php';
